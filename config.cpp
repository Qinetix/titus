#define true	1
#define false	0

#define private		0
#define protected		1
#define public		2

#define TEast		0
#define TWest		1
#define TGuerrila		2
#define TCivilian		3
#define TSideUnknown		4
#define TEnemy		5
#define TFriendly		6
#define TLogic		7

class CfgPatches {
	class QIN_Titus {
		requiredAddons[] = {"A3_Soft_F"};
		requiredVersion = 1.60;
		version = 0.85;
		units[] = {"QIN_Titus_DES", "QIN_Titus_WDL", "QIN_Titus_arx20_DES", "QIN_Titus_arx20_WDL","QIN_Titus_police"};
		weapons[] = {"QIN_10M621_20mm", "QIN_M240C"};
	};
};

class DefaultEventHandlers;	// External class reference
class WeaponFireGun;	// External class reference
class WeaponCloudsGun;	// External class reference
class WeaponFireMGun;	// External class reference
class WeaponCloudsMGun;	// External class reference
class RCWSOptics;	// External class reference

class Mode_FullAuto;	// External class reference

class DefaultVehicleSystemsDisplayManagerLeft {
	class components;	// External class reference
};

class DefaultVehicleSystemsDisplayManagerRight {
	class components;	// External class reference
};

class VehicleSystemsTemplateLeftDriver : DefaultVehicleSystemsDisplayManagerLeft {
	class components;	// External class reference
};

class VehicleSystemsTemplateRightDriver : DefaultVehicleSystemsDisplayManagerRight {
	class components;	// External class reference
};

class VehicleSystemsTemplateLeftCommander : DefaultVehicleSystemsDisplayManagerLeft {
	class components;	// External class reference
};

class VehicleSystemsTemplateRightCommander : DefaultVehicleSystemsDisplayManagerRight {
	class components;	// External class reference
};

class VehicleSystemsTemplateLeftGunner : DefaultVehicleSystemsDisplayManagerLeft {
	class components;	// External class reference
};

class VehicleSystemsTemplateRightGunner : DefaultVehicleSystemsDisplayManagerRight {
	class components;	// External class reference
};

class cfgMagazines {
	class Default;
	class VehicleMagazine : Default {};
	class 300Rnd_20mm_shells : VehicleMagazine {};
	class QIN_139Rnd_20mm_shells : 300Rnd_20mm_shells {
		count = 139;
	};
	class 200Rnd_762x51_Belt : VehicleMagazine {};
	class QIN_300Rnd_762x51_Belt : 200Rnd_762x51_Belt {
		count = 300;
	};
};

class cfgWeapons {
	class Default;	// External class reference
	class MGunCore;	// External class reference
	class CannonCore;	// External class reference

	class gatling_20mm : CannonCore {};

	class MGun : MGunCore {};
	class LMG_RCWS : MGun {};
	class LMG_coax : LMG_RCWS {};

	class QIN_10M621_20mm : gatling_20mm {
		displayName = "10M261 20mm";
		magazines[] = {"QIN_139Rnd_20mm_shells"};
		muzzlePos = "usti hlavne";
		muzzleEnd = "konec hlavne";
		selectionFireAnim = "zasleh";
		ballisticsComputer = 2;

		class manual : MGun {
			class StandardSound {
				begin1[] = {"A3\Sounds_F\arsenal\weapons_vehicles\cannon_30mm\autocannon_30mm_01",1.99526,1,1500};
				soundBegin[] = {"begin1",1};
			};
			displayName = "10M261 20mm";
			autoFire = true;
			sounds[] = {"StandardSound"};
			reloadTime = 0.08;
			dispersion = 0.0022;
			showToPlayer = true;
			burst = 1;
			aiRateOfFire = 0.5;	// delay between shots at given distance
			aiRateOfFireDistance = 50;
			minRange = 1;
			minRangeProbab = 0.01;
			midRange = 2;
			midRangeProbab = 0.01;
			maxRange = 3;
			maxRangeProbab = 0.01;
		};

		class close : manual {
			showToPlayer = false;
			soundBurst = 0;
			burst = 10;
			aiRateOfFire = 0.5;	// delay between shots at given distance
			aiRateOfFireDistance = 50;
			minRange = 0;
			minRangeProbab = 0.05;
			midRange = 20;
			midRangeProbab = 0.58;
			maxRange = 100;
			maxRangeProbab = 0.04;
		};

		class short : close {
			burst = 8;
			aiRateOfFire = 2;	// delay between shots at given distance
			aiRateOfFireDistance = 300;
			minRange = 50;
			minRangeProbab = 0.05;
			midRange = 150;
			midRangeProbab = 0.58;
			maxRange = 300;
			maxRangeProbab = 0.04;
		};

		class medium : close {
			burst = 4;
			aiRateOfFire = 3;	// delay between shots at given distance
			aiRateOfFireDistance = 600;
			minRange = 200;
			minRangeProbab = 0.05;
			midRange = 500;
			midRangeProbab = 0.58;
			maxRange = 800;
			maxRangeProbab = 0.04;
		};

		class far : close {
			burst = 5;
			aiRateOfFire = 5;	// delay between shots at given distance
			aiRateOfFireDistance = 1000;
			minRange = 600;
			minRangeProbab = 0.05;
			midRange = 800;
			midRangeProbab = 0.4;
			maxRange = 1200;
			maxRangeProbab = 0.01;
		};
	};
	class QIN_M240C : LMG_coax {
		displayName = "M240C";
		magazines[] = {"QIN_300Rnd_762x51_Belt"};

        class gunParticles
        {
            class effect1
            {
                positionName = "usti hlavne2";
				directionName = "konec hlavne2";
				effectName = "MachineGunCloud";
            };
        };

		class manual : MGun {
			sounds[] = {"StandardSound"};
			displayName = "M240C";
			autoFire = true;
			reloadTime = 0.075;
			dispersion = 0.0012;
			showToPlayer = true;
			burst = 1;
			aiRateOfFire = 0.5;	// delay between shots at given distance
			aiRateOfFireDistance = 50;
			minRange = 1;
			minRangeProbab = 0.01;
			midRange = 2;
			midRangeProbab = 0.01;
			maxRange = 3;
			maxRangeProbab = 0.01;
		};

		class close : manual {
			showToPlayer = false;
			soundBurst = 0;
			burst = 10;
			aiRateOfFire = 0.5;	// delay between shots at given distance
			aiRateOfFireDistance = 50;
			minRange = 0;
			minRangeProbab = 0.05;
			midRange = 100;
			midRangeProbab = 0.58;
			maxRange = 200;
			maxRangeProbab = 0.04;
		};

		class short : close {
			burst = 8;
			aiRateOfFire = 2;	// delay between shots at given distance
			aiRateOfFireDistance = 300;
			minRange = 50;
			minRangeProbab = 0.05;
			midRange = 200;
			midRangeProbab = 0.58;
			maxRange = 400;
			maxRangeProbab = 0.04;
		};

		class medium : close {
			burst = 4;
			aiRateOfFire = 3;	// delay between shots at given distance
			aiRateOfFireDistance = 600;
			minRange = 300;
			minRangeProbab = 0.05;
			midRange = 400;
			midRangeProbab = 0.58;
			maxRange = 600;
			maxRangeProbab = 0.04;
		};

		class far : close {
			burst = 5;
			aiRateOfFire = 5;	// delay between shots at given distance
			aiRateOfFireDistance = 1000;
			minRange = 500;
			minRangeProbab = 0.05;
			midRange = 600;
			midRangeProbab = 0.4;
			maxRange = 800;
			maxRangeProbab = 0.01;
		};
	};
};

class CfgSounds {
	sounds[] = {"qin_sirena"};

	class qin_sirena {
		name = "Police SIREN (long)";
		sound[] = {"qin_titus\sounds\sirena.ogg", 5, 1.0, 400};
		titles[] = {};
	};
};

class CfgVehicles {
	class LandVehicle;	// External class reference

	class Car : LandVehicle {
		class HitPoints;	// External class reference
		class NewTurret;	// External class reference
	};

	class Car_F : Car {
		class Turrets {
			class MainTurret : NewTurret {
				class Components;	// External class reference
				class ViewOptics;	// External class reference
			};
			class ViewGunner;	// External class reference
		};
	};

	class Truck_F : Car_F {
		unitInfoType = "RscUnitInfoNoWeapon";

		class Exhausts {
			class Exhaust1 {
				position = "exhaust_pos";
				direction = "exhaust_dir";
				effect = "ExhaustEffectHEMTT";
			};
		};

		class HitPoints : HitPoints {
			class HitRGlass;	// External class reference
			class HitLGlass;	// External class reference
			class HitGlass1;	// External class reference
			class HitGlass2;	// External class reference
			class HitGlass3;	// External class reference
			class HitGlass4;	// External class reference
			class HitGlass5;	// External class reference
			class HitGlass6;	// External class reference
			class HitBody;	// External class reference
			class HitFuel;	// External class reference
			class HitLFWheel;	// External class reference
			class HitLBWheel;	// External class reference
			class HitLMWheel;	// External class reference
			class HitLF2Wheel;	// External class reference
			class HitRFWheel;	// External class reference
			class HitRBWheel;	// External class reference
			class HitRMWheel;	// External class reference
			class HitRF2Wheel;	// External class reference
			class HitEngine;	// External class reference
		};
		class EventHandlers;	// External class reference
		class AnimationSources;	// External class reference
	};

	class QIN_Titus_base : Truck_F {
		author = "Qinetix";
		mapSize = 7.72;
		_generalMacro = "QIN_Titus_base";

		scope = 0;
		class EventHandlers: EventHandlers
		{
			// (_this select 0): the vehicle
			// """" Random texture source (pick one from the property textureList[])
			// []: randomize the animation sources (accordingly to the property animationList[])
			// false: Don't change the mass even if an animation source has a defined mass
			init="if (local (_this select 0)) then {[(_this select 0), """", [], false] call bis_fnc_initVehicle;};";
		};

		transportMaxBackpacks = 25;
		vehicleClass = "Car";
		transportSoldier = 8;
		crewVulnerable = false;
		crewCrashProtection = 1.35;
		crewExplosionProtection = 0.9999;
		maxFordingDepth = -0.1;
		enableRadio = 1;
		enableGPS = 1;
		armor = 300;
		armorStructural = 5;
		cost = 500000;

		tf_hasLRradio_api = 1;

		cargoProxyIndexes[] = {1, 2, 3, 4, 5, 6, 7, 8};
		getInProxyOrder[] = {11, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

		memoryPointsGetInCargo[] = {"pos cargo"};
		memoryPointsGetInCargoDir[] = {"pos cargo dir"};

		selectionDashboard = "podsvit pristroju";

		class Library {
			libTextDesc = "";
		};

		editorSubcategory = "EdSubcat_Cars";

		// threat (VSoft, VArmor, VAir), how threatening vehicle is to unit types
		threat[] = {0.8, 0.6, 0.3};

		memoryPointTaskMarker = "TaskMarker_1_pos";
		slingLoadCargoMemoryPoints[] = {"SlingLoadCargo1", "SlingLoadCargo2", "SlingLoadCargo3", "SlingLoadCargo4"};



		class HitPoints : HitPoints {
			class HitFuel : HitFuel {
				armor = 1.5;
				passThrough = 0.2;
				minimalHit = 0.0;
				explosionShielding = 0.5;
				radius = 0.45;
			};

			class HitEngine : HitEngine {
				armor = 0.9;
				passThrough = 0.2;
				minimalHit = 0.1;
				explosionShielding = 0.5;
				radius = 0.45;
			};

			class HitBody : HitBody {
				armor = 6.0;
				passThrough = false;
				minimalHit = 0.05;
				explosionShielding = 0.01;
				radius = 0.22;
			};

			class HitLFWheel : HitLFWheel {
				armor = 0.5;
				minimalHit = 0.02;
				passThrough = 0.3;
				explosionShielding = 4;
				radius = 0.33;
			};

			class HitLBWheel : HitLBWheel {
				armor = 0.66;
				minimalHit = 0.02;
				passThrough = 0.3;
				explosionShielding = 4;
				radius = 0.33;
			};

			class HitLMWheel : HitLMWheel {
				armor = 0.66;
				minimalHit = 0.02;
				passThrough = 0.3;
				explosionShielding = 4;
				radius = 0.33;
			};

			class HitLF2Wheel : HitLF2Wheel {
				armor = 0.66;
				minimalHit = 0.02;
				passThrough = 0.3;
				explosionShielding = 4;
				radius = 0.33;
			};

			class HitRFWheel : HitRFWheel {
				armor = 0.66;
				minimalHit = 0.02;
				passThrough = 0.3;
				explosionShielding = 4;
				radius = 0.33;
			};

			class HitRBWheel : HitRBWheel {
				armor = 0.66;
				minimalHit = 0.02;
				passThrough = 0.3;
				explosionShielding = 4;
				radius = 0.33;
			};

			class HitRMWheel : HitRMWheel {
				armor = 0.66;
				minimalHit = 0.02;
				passThrough = 0.3;
				explosionShielding = 4;
				radius = 0.33;
			};

			class HitRF2Wheel : HitRF2Wheel {
				armor = 0.66;
				minimalHit = 0.02;
				passThrough = 0.3;
				explosionShielding = 4;
				radius = 0.33;
			};

			class HitGlass1 : HitGlass1 {
				armor = 2;
				passThrough = false;
				explosionShielding = 3;
				radius = 0.25;
			};

			class HitGlass2 : HitGlass2 {
				armor = 2;
				passThrough = false;
				explosionShielding = 3;
				radius = 0.25;
			};

			class HitGlass3 : HitGlass3 {
				armor = 2;
				passThrough = false;
				explosionShielding = 3;
				radius = 0.25;
			};

			class HitGlass4 : HitGlass4 {
				armor = 2;
				passThrough = false;
				explosionShielding = 3;
				radius = 0.25;
			};
			class HitGlass5 : HitGlass5 {
				armor = 2;
				passThrough = false;
				explosionShielding = 3;
				radius = 0.25;
			};
			class HitGlass6 : HitGlass6 {
				armor = 2;
				passThrough = false;
				explosionShielding = 3;
				radius = 0.25;
			};
			class HitGlass7 : HitGlass6 {
				armor = 2;
				passThrough = false;
				explosionShielding = 3;
				radius = 0.25;
			};
			class HitSLAT_Left_1
		    {
		        simulation = "Armor_SLAT";
		        armorComponent = "cage_left_1";
		        name = "cage_left_1_point";
		        armor = -200;
		        minimalHit = 0.3;
		        passThrough = 0;
		        visual = "-";
		        explosionShielding = 2;
		        radius = 0.25;
		    };
		    class HitSLAT_Left_2 : HitSLAT_Left_1
		    {
		    	armorComponent = "cage_left_2";
		        name = "cage_left_2_point";
		    };
		    class HitSLAT_Left_3 : HitSLAT_Left_1
		    {
		    	armorComponent = "cage_left_3";
		        name = "cage_left_3_point";
		    };
		    class HitSLAT_Left_4 : HitSLAT_Left_1
		    {
		    	armorComponent = "cage_left_4";
		        name = "cage_left_4_point";
		    };
		    class HitSLAT_Left_5 : HitSLAT_Left_1
		    {
		    	armorComponent = "cage_left_5";
		        name = "cage_left_5_point";
		    };
		    class HitSLAT_Right_1 : HitSLAT_Left_1
		    {
		    	armorComponent = "cage_right_1";
		        name = "cage_right_1_point";
		    };
		    class HitSLAT_Right_2 : HitSLAT_Left_1
		    {
		    	armorComponent = "cage_right_2";
		        name = "cage_right_2_point";
		    };
		    class HitSLAT_Right_3 : HitSLAT_Left_1
		    {
		    	armorComponent = "cage_right_3";
		        name = "cage_right_3_point";
		    };
		    class HitSLAT_Right_4 : HitSLAT_Left_1
		    {
		    	armorComponent = "cage_right_4";
		        name = "cage_right_4_point";
		    };
		    class HitSLAT_Right_5 : HitSLAT_Left_1
		    {
		    	armorComponent = "cage_right_5";
		        name = "cage_right_5_point";
		    };
		    class HitSLAT_Front_1 : HitSLAT_Left_1
		    {
		    	armorComponent = "cage_front_1";
		        name = "cage_front_1_point";
		    };
		    class HitSLAT_Front_2 : HitSLAT_Left_1
		    {
		    	armorComponent = "cage_front_2";
		        name = "cage_front_2_point";
		    };
		    class HitSLAT_Rear_1 : HitSLAT_Left_1
		    {
		    	armorComponent = "cage_rear_1";
		        name = "cage_rear_1_point";
		    };
		    class HitSLAT_Rear_2 : HitSLAT_Left_1
		    {
		    	armorComponent = "cage_rear_2";
		        name = "cage_rear_2_point";
		    };
		    class HitSLAT_Rear_3 : HitSLAT_Left_1
		    {
		    	armorComponent = "cage_rear_3";
		        name = "cage_rear_3_point";
		    };
		};

		wheelDamageThreshold = 0.7;
		wheelDestroyThreshold = 0.99;
		wheelDamageRadiusCoef = 0.95;
		wheelDestroyRadiusCoef = 0.45;
		weapons[] = {"TruckHorn2"};
		driverAction = "Driver_low01";
		driverInAction = "Driver_low01";
		forcehideDriver = true;
		cargoAction[] = {"passenger_generic01_foldhands", "passenger_apc_generic02", "passenger_apc_generic04", "passenger_apc_generic03", "passenger_apc_generic01", "passenger_apc_narrow_generic01", "passenger_generic01_foldhands", "passenger_generic01_leanleft", "passenger_generic01_leanright", "passenger_apc_generic01", "passenger_generic01_leanleft", "passenger_generic01_foldhands", "passenger_apc_generic02", "passenger_apc_generic03", "passenger_apc_narrow_generic01", "passenger_apc_narrow_generic03", "passenger_generic01_foldhands"};
		cargoIsCoDriver[] = {1, 0};
		cargoGetOutAction[] = {"GetOutMedium"};
		getInAction = "GetInMRAP_01";
		getOutAction = "GetOutLow";
		cargoGetInAction[] = {"GetInMRAP_01"};
		commanderCanSee = 31;
		hideWeaponsDriver = true;
		hideWeaponsCargo = true;
		turnCoef = 3;
		terrainCoef = 1.5;
		driverDoor = "door_lf";
		cargoDoors[] = {"door_rear"};
		driverCompartments = "Compartment2";
		cargoCompartments[] = {"Compartment2"};
		destrType = "DestructDefault";
		showNVGCargo[] = {0, 1};
		soundAttenuationCargo[] = {1, 0};
		attenuationEffectType = "CarAttenuation";
		soundGetIn[] = {"A3\Sounds_F\vehicles\soft\Truck_02\getin", 0.562341, 1};
		soundGetOut[] = {"A3\Sounds_F\vehicles\soft\Truck_02\getout", 0.562341, 1, 20};
		soundDammage[] = {"", 0.562341, 1};
		soundEngineOnInt[] = {"A3\Sounds_F\vehicles\soft\Truck_02\int_start", 0.158489, 1.0};
		soundEngineOffInt[] = {"A3\Sounds_F\vehicles\soft\Truck_02\int_stop", 0.199526, 1.0};
		soundEngineOnExt[] = {"A3\Sounds_F\vehicles\soft\Truck_02\ext_start", 1.0, 1.0, 200};
		soundEngineOffExt[] = {"A3\Sounds_F\vehicles\soft\Truck_02\ext_stop", 1.0, 1.0, 200};
		buildCrash0[] = {"A3\Sounds_F\vehicles\crashes\cars\cars_coll_big_default_ext_1", 1.0, 1, 200};
		buildCrash1[] = {"A3\Sounds_F\vehicles\crashes\cars\cars_coll_big_default_ext_2", 1.0, 1, 200};
		buildCrash2[] = {"A3\Sounds_F\vehicles\crashes\cars\cars_coll_big_default_ext_3", 1.0, 1, 200};
		buildCrash3[] = {"A3\Sounds_F\vehicles\crashes\cars\cars_coll_big_default_ext_4", 1.0, 1, 200};
		soundBuildingCrash[] = {"buildCrash0", 0.25, "buildCrash1", 0.25, "buildCrash2", 0.25, "buildCrash3", 0.25};
		WoodCrash0[] = {"A3\Sounds_F\vehicles\crashes\cars\cars_coll_big_wood_ext_1", 1.0, 1, 200};
		WoodCrash1[] = {"A3\Sounds_F\vehicles\crashes\cars\cars_coll_big_wood_ext_1", 1.0, 1, 200};
		WoodCrash2[] = {"A3\Sounds_F\vehicles\crashes\cars\cars_coll_big_wood_ext_1", 1.0, 1, 200};
		WoodCrash3[] = {"A3\Sounds_F\vehicles\crashes\cars\cars_coll_big_wood_ext_1", 1.0, 1, 200};
		soundWoodCrash[] = {"woodCrash0", 0.25, "woodCrash1", 0.25, "woodCrash2", 0.25, "woodCrash3", 0.25};
		armorCrash0[] = {"A3\Sounds_F\vehicles\crashes\cars\cars_coll_big_default_ext_1", 1.0, 1, 200};
		armorCrash1[] = {"A3\Sounds_F\vehicles\crashes\cars\cars_coll_big_default_ext_2", 1.0, 1, 200};
		armorCrash2[] = {"A3\Sounds_F\vehicles\crashes\cars\cars_coll_big_default_ext_3", 1.0, 1, 200};
		armorCrash3[] = {"A3\Sounds_F\vehicles\crashes\cars\cars_coll_big_default_ext_4", 1.0, 1, 200};
		soundArmorCrash[] = {"ArmorCrash0", 0.25, "ArmorCrash1", 0.25, "ArmorCrash2", 0.25, "ArmorCrash3", 0.25};
		Crash0[] = {"A3\Sounds_F\vehicles\crashes\cars\cars_coll_big_default_ext_1", 1.0, 1, 200};
		Crash1[] = {"A3\Sounds_F\vehicles\crashes\cars\cars_coll_big_default_ext_2", 1.0, 1, 200};
		Crash2[] = {"A3\Sounds_F\vehicles\crashes\cars\cars_coll_big_default_ext_3", 1.0, 1, 200};
		Crash3[] = {"A3\Sounds_F\vehicles\crashes\cars\cars_coll_big_default_ext_4", 1.0, 1, 200};
		soundCrashes[] = {"Crash0", 0.25, "Crash1", 0.25, "Crash2", 0.25, "Crash3", 0.25};

		class Sounds {
			class Idle_int {
				sound[] = {"A3\Sounds_F\vehicles\soft\Truck_02\int_engine_00", db-10, 1};
				frequency = "0.8	+	((rpm/	3000) factor[(100/	3000),(800/	3000)])*0.15";
				volume = "engineOn*(1-camPos)*(((rpm/	3000) factor[(10/	3000),(200/	3000)])	*	((rpm/	3000) factor[(600/	3000),(400/	3000)]))";
			};

			class Engine_int {
				sound[] = {"A3\Sounds_F\vehicles\soft\Truck_02\int_engine_01", 0.281838, 1};
				frequency = "0.8	+	((rpm/	3000) factor[(610/	3000),(1200/	3000)])*0.2";
				volume = "engineOn*(1-camPos)*(((rpm/	3000) factor[(450/	3000),(820/	3000)])	*	((rpm/	3000) factor[(1200/	3000),(1000/	3000)]))";
			};

			class Engine1_int {
				sound[] = {"A3\Sounds_F\vehicles\soft\Truck_02\int_engine_02", 0.281838, 1};
				frequency = "0.8	+	((rpm/	3000) factor[(1000/	3000),(1400/	3000)])*0.2";
				volume = "engineOn*(1-camPos)*(((rpm/	3000) factor[(900/	3000),(1200/	3000)])	*	((rpm/	3000) factor[(1550/	3000),(1300/	3000)]))";
			};

			class Engine2_int {
				sound[] = {"A3\Sounds_F\vehicles\soft\Truck_02\int_engine_03", 0.281838, 1};
				frequency = "0.8	+	((rpm/	3000) factor[(1200/	3000),(1600/	3000)])*0.2";
				volume = "engineOn*(1-camPos)*(((rpm/	3000) factor[(1200/	3000),(1400/	3000)])	*	((rpm/	3000) factor[(1650/	3000),(1500/	3000)]))";
			};

			class Engine3_int {
				sound[] = {"A3\Sounds_F\vehicles\soft\Truck_02\int_engine_04", 0.354813, 1};
				frequency = "0.8	+	((rpm/	3000) factor[(1400/	3000),(2200/	3000)])*0.2";
				volume = "engineOn*(1-camPos)*(((rpm/	3000) factor[(1300/	3000),(1500/	3000)])	*	((rpm/	3000) factor[(2200/	3000),(1600/	3000)]))*1.3";
			};

			class Engine4_int {
				sound[] = {"A3\Sounds_F\vehicles\soft\Truck_02\int_engine_05", 0.354813, 1};
				frequency = "0.8	+	((rpm/	3000) factor[(1800/	3000),(2700/	3000)])*0.2";
				volume = "engineOn*(1-camPos)*(((rpm/	3000) factor[(1800/	3000),(2200/	3000)])	*	((rpm/	3000) factor[(2750/	3000),(2400/	3000)]))*1.3";
			};

			class Engine5_int {
				sound[] = {"A3\Sounds_F\vehicles\soft\Truck_02\int_engine_06", 0.398107, 1};
				frequency = "0.8	+	((rpm/	3000) factor[(2300/	3000),(3000/	3000)])*0.2";
				volume = "engineOn*(1-camPos)*((rpm/	3000) factor[(2300/	3000),(3000/	3000)])";
			};

			class Idle_ext {
				sound[] = {"A3\Sounds_F\vehicles\soft\Truck_02\ext_engine_00", 0.281838, 1};
				frequency = "0.9	+	((rpm/	3000) factor[(100/	3000),(800/	3000)])*0.15";
				volume = "engineOn*camPos*(((rpm/	3000) factor[(10/	3000),(200/	3000)])	*	((rpm/	3000) factor[(600/	3000),(400/	3000)]))";
			};

			class Engine_ext {
				sound[] = {"A3\Sounds_F\vehicles\soft\Truck_02\ext_engine_01", db-10, 1};
				frequency = "0.9	+	((rpm/	3000) factor[(610/	3000),(1200/	3000)])*0.2";
				volume = "engineOn*camPos*(((rpm/	3000) factor[(450/	3000),(820/	3000)])	*	((rpm/	3000) factor[(1200/	3000),(1000/	3000)]))";
			};

			class Engine1_ext {
				sound[] = {"A3\Sounds_F\vehicles\soft\Truck_02\ext_engine_02", 0.354813, 1};
				frequency = "0.9	+	((rpm/	3000) factor[(1000/	3000),(1400/	3000)])*0.2";
				volume = "engineOn*camPos*(((rpm/	3000) factor[(900/	3000),(1200/	3000)])	*	((rpm/	3000) factor[(1550/	3000),(1300/	3000)]))";
			};

			class Engine2_ext {
				sound[] = {"A3\Sounds_F\vehicles\soft\Truck_02\ext_engine_03", 0.354813, 1};
				frequency = "0.9	+	((rpm/	3000) factor[(1200/	3000),(1600/	3000)])*0.2";
				volume = "engineOn*camPos*(((rpm/	3000) factor[(1200/	3000),(1400/	3000)])	*	((rpm/	3000) factor[(1650/	3000),(1500/	3000)]))";
			};

			class Engine3_ext {
				sound[] = {"A3\Sounds_F\vehicles\soft\Truck_02\ext_engine_04", 0.398107, 1};
				frequency = "0.9	+	((rpm/	3000) factor[(1400/	3000),(2200/	3000)])*0.2";
				volume = "engineOn*camPos*(((rpm/	3000) factor[(1300/	3000),(1500/	3000)])	*	((rpm/	3000) factor[(2200/	3000),(1600/	3000)]))*1.3";
			};

			class Engine4_ext {
				sound[] = {"A3\Sounds_F\vehicles\soft\Truck_02\ext_engine_05", 0.398107, 1};
				frequency = "0.9	+	((rpm/	3000) factor[(1800/	3000),(2700/	3000)])*0.2";
				volume = "engineOn*camPos*(((rpm/	3000) factor[(1800/	3000),(2200/	3000)])	*	((rpm/	3000) factor[(2750/	3000),(2400/	3000)]))*1.3";
			};

			class Engine5_ext {
				sound[] = {"A3\Sounds_F\vehicles\soft\Truck_02\ext_engine_06", db-7, 1};
				frequency = "0.9	+	((rpm/	3000) factor[(2300/	3000),(3000/	3000)])*0.2";
				volume = "engineOn*camPos*((rpm/	3000) factor[(2300/	3000),(3000/	3000)])";
			};

			class IdleThrust {
				sound[] = {"A3\Sounds_F\vehicles\soft\Truck_02\ext_exhaust_00", 0.630957, 1, 250};
				frequency = "0.9	+	((rpm/	3000) factor[(100/	3000),(800/	3000)])*0.15";
				volume = "engineOn*camPos*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/	3000) factor[(10/	3000),(200/	3000)])	*	((rpm/	3000) factor[(600/	3000),(400/	3000)]))";
			};

			class EngineThrust {
				sound[] = {"A3\Sounds_F\vehicles\soft\Truck_02\ext_exhaust_01", db-2, 1, 300};
				frequency = "0.9	+	((rpm/	3000) factor[(610/	3000),(1200/	3000)])*0.2";
				volume = "engineOn*camPos*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/	3000) factor[(450/	3000),(820/	3000)])	*	((rpm/	3000) factor[(1200/	3000),(1000/	3000)]))";
			};

			class Engine1_Thrust_ext {
				sound[] = {"A3\Sounds_F\vehicles\soft\Truck_02\ext_exhaust_02", db-3, 1, 350};
				frequency = "0.9	+	((rpm/	3000) factor[(1000/	3000),(1400/	3000)])*0.2";
				volume = "engineOn*camPos*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/	3000) factor[(900/	3000),(1200/	3000)])	*	((rpm/	3000) factor[(1550/	3000),(1300/	3000)]))";
			};

			class Engine2_Thrust_ext {
				sound[] = {"A3\Sounds_F\vehicles\soft\Truck_02\ext_exhaust_03", db0, 1, 400};
				frequency = "0.9	+	((rpm/	3000) factor[(1200/	3000),(1600/	3000)])*0.2";
				volume = "engineOn*camPos*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/	3000) factor[(1200/	3000),(1400/	3000)])	*	((rpm/	3000) factor[(1650/	3000),(1500/	3000)]))";
			};

			class Engine3_Thrust_ext {
				sound[] = {"A3\Sounds_F\vehicles\soft\Truck_02\ext_exhaust_04", db2, 1, 450};
				frequency = "0.9	+	((rpm/	3000) factor[(1400/	3000),(2200/	3000)])*0.1";
				volume = "engineOn*camPos*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/	3000) factor[(1300/	3000),(1500/	3000)])	*	((rpm/	3000) factor[(2200/	3000),(1600/	3000)]))";
			};

			class Engine4_Thrust_ext {
				sound[] = {"A3\Sounds_F\vehicles\soft\Truck_02\ext_exhaust_05", db2, 1, 450};
				frequency = "0.9	+	((rpm/	3000) factor[(1800/	3000),(2700/	3000)])*0.1";
				volume = "engineOn*camPos*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/	3000) factor[(1800/	3000),(2200/	3000)])	*	((rpm/	3000) factor[(2750/	3000),(2400/	3000)]))";
			};

			class Engine5_Thrust_ext {
				sound[] = {"A3\Sounds_F\vehicles\soft\Truck_02\ext_exhaust_06", db3, 1, 500};
				frequency = "0.9	+	((rpm/	3000) factor[(2300/	3000),(3000/	3000)])*0.1";
				volume = "engineOn*camPos*(0.4+(0.6*(thrust factor[0.1,1])))*((rpm/	3000) factor[(2300/	3000),(3000/	3000)])";
			};

			class IdleThrust_int {
				sound[] = {"A3\Sounds_F\vehicles\soft\Truck_02\int_exhaust_00", 0.630957, 1};
				frequency = "0.8	+	((rpm/	3000) factor[(100/	3000),(800/	3000)])*0.15";
				volume = "engineOn*(1-camPos)*(((rpm/	3000) factor[(10/	3000),(200/	3000)])	*	((rpm/	3000) factor[(600/	3000),(400/	3000)]))";
			};

			class EngineThrust_int {
				sound[] = {"A3\Sounds_F\vehicles\soft\Truck_02\int_exhaust_01", db-13, 1};
				frequency = "0.8	+	((rpm/	3000) factor[(610/	3000),(1200/	3000)])*0.2";
				volume = "engineOn*(1-camPos)*(((rpm/	3000) factor[(450/	3000),(820/	3000)])	*	((rpm/	3000) factor[(1200/	3000),(1000/	3000)]))";
			};

			class Engine1_Thrust_int {
				sound[] = {"A3\Sounds_F\vehicles\soft\Truck_02\int_exhaust_02", db-12};
				frequency = "0.8	+	((rpm/	3000) factor[(1000/	3000),(1400/	3000)])*0.2";
				volume = "engineOn*(1-camPos)*(((rpm/	3000) factor[(900/	3000),(1200/	3000)])	*	((rpm/	3000) factor[(1550/	3000),(1300/	3000)]))";
			};

			class Engine2_Thrust_int {
				sound[] = {"A3\Sounds_F\vehicles\soft\Truck_02\int_exhaust_03", db-12, 1};
				frequency = "0.8	+	((rpm/	3000) factor[(1200/	3000),(1600/	3000)])*0.2";
				volume = "engineOn*(1-camPos)*(((rpm/	3000) factor[(1200/	3000),(1400/	3000)])	*	((rpm/	3000) factor[(1650/	3000),(1500/	3000)]))";
			};

			class Engine3_Thrust_int {
				sound[] = {"A3\Sounds_F\vehicles\soft\Truck_02\int_exhaust_04", 0.281838, 1};
				frequency = "0.8	+	((rpm/	3000) factor[(1400/	3000),(2200/	3000)])*0.1";
				volume = "engineOn*(1-camPos)*(((rpm/	3000) factor[(1300/	3000),(1500/	3000)])	*	((rpm/	3000) factor[(2200/	3000),(1600/	3000)]))";
			};

			class Engine4_Thrust_int {
				sound[] = {"A3\Sounds_F\vehicles\soft\Truck_02\int_exhaust_05", db-10, 1};
				frequency = "0.8	+	((rpm/	3000) factor[(1800/	3000),(2700/	3000)])*0.1";
				volume = "engineOn*(1-camPos)*(((rpm/	3000) factor[(1800/	3000),(2200/	3000)])	*	((rpm/	3000) factor[(2750/	3000),(2400/	3000)]))";
			};

			class Engine5_Thrust_int {
				sound[] = {"A3\Sounds_F\vehicles\soft\Truck_02\int_exhaust_06", db-3, 1};
				frequency = "0.8	+	((rpm/	3000) factor[(2300/	3000),(3000/	3000)])*0.1";
				volume = "engineOn*(1-camPos)*((rpm/	3000) factor[(2300/	3000),(3000/	3000)])";
			};

			class TiresRockOut {
				sound[] = {"A3\Sounds_F\vehicles\soft\tires\ext_tires_dirt_soft_1", db3, 1.0, 60};
				frequency = "1";
				volume = "camPos*rock*(speed factor[0, 8])";
			};

			class TiresSandOut {
				sound[] = {"A3\Sounds_F\vehicles\soft\tires\ext-tires-sand1", db3, 1.0, 60};
				frequency = "1";
				volume = "camPos*sand*(speed factor[0, 8])";
			};

			class TiresGrassOut {
				sound[] = {"A3\Sounds_F\vehicles\soft\Truck_01\Truck_01_Tires_Mud", db6, 1.0, 60};
				frequency = "1";
				volume = "camPos*grass*(speed factor[0, 8])";
			};

			class TiresGrassOut_DirtLayer {
				sound[] = {"A3\Sounds_F\vehicles\soft\Truck_01\Truck_01_Tires_Dirt_Ext", db9, 1.0, 60};
				frequency = "1";
				volume = "camPos*grass*(speed factor[0, 8])";
			};

			class TiresMudOut {
				sound[] = {"A3\Sounds_F\vehicles\soft\Truck_01\Truck_01_Tires_Mud", db6, 1.0, 60};
				frequency = "1";
				volume = "camPos*mud*(speed factor[0, 8])";
			};

			class TiresMudOut_DirtLayer {
				sound[] = {"A3\Sounds_F\vehicles\soft\Truck_01\Truck_01_Tires_Dirt_Ext", db9, 1.0, 60};
				frequency = "1";
				volume = "camPos*mud*(speed factor[0, 8])";
			};

			class TiresGravelOut {
				sound[] = {"A3\Sounds_F\vehicles\soft\tires\ext_tires_gravel_1", db3, 1.0, 60};
				frequency = "1";
				volume = "camPos*gravel*(speed factor[0, 8])";
			};

			class TiresAsphaltOut {
				sound[] = {"A3\Sounds_F\vehicles\soft\Truck_01\Truck_01_Tires_Asphalt", 2.51189, 1.0, 60};
				frequency = "1";
				volume = "camPos*asphalt*(speed factor[0, 8])";
			};

			class NoiseOut {
				sound[] = {"A3\Sounds_F\vehicles\soft\noises\noise_int_car_3", db6, 1.0, 90};
				frequency = "1";
				volume = "camPos*(damper0 max 0.02)*(speed factor[0, 8])";
			};

			class TiresRockOut_Reverse {
				sound[] = {"A3\Sounds_F\vehicles\soft\tires\ext_tires_dirt_soft_1", db3, 1.0, 60};
				frequency = "1";
				volume = "camPos*rock*(speed factor[0,-2])";
			};

			class TiresSandOut_Reverse {
				sound[] = {"A3\Sounds_F\vehicles\soft\tires\ext-tires-sand1", db3, 1.0, 60};
				frequency = "1";
				volume = "camPos*sand*(speed factor[0,-2])";
			};

			class TiresGrassOut_Reverse {
				sound[] = {"A3\Sounds_F\vehicles\soft\Truck_01\Truck_01_Tires_Mud", db6, 1.0, 60};
				frequency = "1";
				volume = "camPos*grass*(speed factor[0,-2])";
			};

			class TiresGrassOut_Reverse_DirtLayer {
				sound[] = {"A3\Sounds_F\vehicles\soft\Truck_01\Truck_01_Tires_Dirt_Ext", db9, 1.0, 60};
				frequency = "1";
				volume = "camPos*grass*(speed factor[0,-2])";
			};

			class TiresMudOut_Reverse {
				sound[] = {"A3\Sounds_F\vehicles\soft\Truck_01\Truck_01_Tires_Mud", db6, 1.0, 60};
				frequency = "1";
				volume = "camPos*mud*(speed factor[0,-2])";
			};

			class TiresMudOut_DirtLayer_Reverse {
				sound[] = {"A3\Sounds_F\vehicles\soft\Truck_01\Truck_01_Tires_Dirt_Ext", db9, 1.0, 60};
				frequency = "1";
				volume = "camPos*mud*(speed factor[0,-2])";
			};

			class TiresGravelOut_Reverse {
				sound[] = {"A3\Sounds_F\vehicles\soft\tires\ext_tires_gravel_1", db3, 1.0, 60};
				frequency = "1";
				volume = "camPos*gravel*(speed factor[0,-2])";
			};

			class TiresAsphaltOut_Reverse {
				sound[] = {"A3\Sounds_F\vehicles\soft\Truck_01\Truck_01_Tires_Asphalt", 2.51189, 1.0, 60};
				frequency = "1";
				volume = "camPos*asphalt*(speed factor[0,-2])";
			};

			class NoiseOut_Reverse {
				sound[] = {"A3\Sounds_F\vehicles\soft\noises\noise_int_car_3", db4, 1.0, 90};
				frequency = "1";
				volume = "camPos*(damper0 max 0.02)*(speed factor[0,-2])";
			};

			class TiresRockIn {
				sound[] = {"A3\Sounds_F\vehicles\soft\tires\int_tires_dirt_soft_1", db-2, 1.0};
				frequency = "1";
				volume = "(1-camPos)*rock*(speed factor[0, 10])";
			};

			class TiresSandIn {
				sound[] = {"A3\Sounds_F\vehicles\soft\tires\int-tires-sand2", db-2, 1.0};
				frequency = "1";
				volume = "(1-camPos)*sand*(speed factor[0, 10])";
			};

			class TiresGrassIn {
				sound[] = {"A3\Sounds_F\vehicles\soft\Truck_01\Truck_01_Tires_Mud", db-6, 1.0};
				frequency = "1";
				volume = "(1-camPos)*grass*(speed factor[0, 10])";
			};

			class TiresMudIn {
				sound[] = {"A3\Sounds_F\vehicles\soft\Truck_01\Truck_01_Tires_Mud", db-6, 1.0};
				frequency = "1";
				volume = "(1-camPos)*mud*(speed factor[0, 10])";
			};

			class TiresGravelIn {
				sound[] = {"A3\Sounds_F\vehicles\soft\tires\int_tires_gravel_1", 0.630957, 1.0};
				frequency = "1";
				volume = "(1-camPos)*gravel*(speed factor[0, 10])";
			};

			class TiresAsphaltIn {
				sound[] = {"A3\Sounds_F\vehicles\soft\Truck_01\Truck_01_Tires_Asphalt", 0.354813, 1.0};
				frequency = "1";
				volume = "(1-camPos)*asphalt*(speed factor[0, 10])";
			};

			class NoiseIn {
				sound[] = {"A3\Sounds_F\vehicles\soft\noises\noise_int_car_3", db-13, 1.0};
				frequency = "1";
				volume = "(damper0 max 0.1)*(speed factor[0, 8])*(1-camPos)";
			};

			class TiresRockIn_Reverse {
				sound[] = {"A3\Sounds_F\vehicles\soft\tires\int_tires_dirt_soft_1", db-2, 1.0};
				frequency = "1";
				volume = "(1-camPos)*rock*(speed factor[0,-2])";
			};

			class TiresSandIn_Reverse {
				sound[] = {"A3\Sounds_F\vehicles\soft\tires\int-tires-sand2", db-2, 1.0};
				frequency = "1";
				volume = "(1-camPos)*sand*(speed factor[0,-2])";
			};

			class TiresGrassIn_Reverse {
				sound[] = {"A3\Sounds_F\vehicles\soft\Truck_01\Truck_01_Tires_Mud", db-6, 1.0};
				frequency = "1";
				volume = "(1-camPos)*grass*(speed factor[0,-2])";
			};

			class TiresMudIn_Reverse {
				sound[] = {"A3\Sounds_F\vehicles\soft\Truck_01\Truck_01_Tires_Mud", db-6, 1.0};
				frequency = "1";
				volume = "(1-camPos)*mud*(speed factor[0,-2])";
			};

			class TiresGravelIn_Reverse {
				sound[] = {"A3\Sounds_F\vehicles\soft\tires\int_tires_gravel_1", 0.630957, 1.0};
				frequency = "1";
				volume = "(1-camPos)*gravel*(speed factor[0,-2])";
			};

			class TiresAsphaltIn_Reverse {
				sound[] = {"A3\Sounds_F\vehicles\soft\Truck_01\Truck_01_Tires_Asphalt", 0.354813, 1.0};
				frequency = "1";
				volume = "(1-camPos)*asphalt*(speed factor[0,-2])";
			};

			class NoiseIn_Reverse {
				sound[] = {"A3\Sounds_F\vehicles\soft\noises\noise_int_car_3", db-13, 1.0};
				frequency = "1";
				volume = "(damper0 max 0.1)*(speed factor[0,-2])*(1-camPos)";
			};

			class brakes_ext_road {
				sound[] = {"A3\Sounds_F\vehicles\soft\noises\slipping_tires_loop_04", db9, 1, 80};
				frequency = 1;
				volume = "1.1*engineOn*camPos*asphalt*(LongSlipDrive factor[-0.02, -0.05])*(Speed factor[0, 15])";
			};

			class turn_left_ext_road {
				sound[] = {"A3\Sounds_F\vehicles\soft\noises\slipping_tires_loop_02", db6, 1, 80};
				frequency = 1;
				volume = "engineOn*camPos*asphalt*(latSlipDrive factor[0, 0.1])*(Speed factor[0, 15])";
			};

			class turn_right_ext_road {
				sound[] = {"A3\Sounds_F\vehicles\soft\noises\slipping_tires_loop_02", db6, 1, 80};
				frequency = 1;
				volume = "engineOn*camPos*asphalt*(latSlipDrive factor[0, -0.1])*(Speed factor[0, 15])";
			};

			class brakes_int_dirt {
				sound[] = {"A3\Sounds_F\vehicles\soft\noises\slipping_tires_14_dirt_breaking_int", db-6, 1};
				frequency = 1;
				volume = "engineOn*(1-asphalt)*(1-camPos)*(LongSlipDrive factor[-0.02, -0.05])*(Speed factor[2, 11])";
			};

			class turn_left_int_dirt {
				sound[] = {"A3\Sounds_F\vehicles\soft\noises\slipping_tires_18_dirt_int", 0.354813, 1};
				frequency = 1;
				volume = "engineOn*(1-asphalt)*(1-camPos)*(latSlipDrive factor[0, 0.1])*(Speed factor[0, 10])";
			};

			class turn_right_int_dirt {
				sound[] = {"A3\Sounds_F\vehicles\soft\noises\slipping_tires_18_dirt_int", 0.354813, 1};
				frequency = 1;
				volume = "engineOn*(1-asphalt)*(1-camPos)*(latSlipDrive factor[0, -0.1])*(Speed factor[0, 10])";
			};

			class brakes_ext_dirt_HI {
				sound[] = {"A3\Sounds_F\vehicles\soft\noises\slipping_tires_15_dirt_breaking", db12, 1, 60};
				frequency = 1;
				volume = "engineOn*camPos*(1-asphalt)*(LongSlipDrive factor[-0.01, -0.1])*(Speed factor[0, 5])";
			};

			class brakes_ext_dirt_LO {
				sound[] = {"A3\Sounds_F\vehicles\soft\noises\slipping_tires_14_dirt_breaking", db12, 1, 60};
				frequency = 1;
				volume = "engineOn*camPos*(1-asphalt)*(LongSlipDrive factor[-0.01, -0.1])*(Speed factor[0, 5])";
			};

			class turn_left_ext_dirt {
				sound[] = {"A3\Sounds_F\vehicles\soft\noises\slipping_tires_18_dirt", db-3, 1, 60};
				frequency = 1;
				volume = "engineOn*camPos*(1-asphalt)*(latSlipDrive factor[0, 0.1])*(Speed factor[0, 10])";
			};

			class turn_right_ext_dirt {
				sound[] = {"A3\Sounds_F\vehicles\soft\noises\slipping_tires_18_dirt", db-3, 1, 60};
				frequency = 1;
				volume = "engineOn*camPos*(1-asphalt)*(latSlipDrive factor[0, -0.1])*(Speed factor[0, 10])";
			};

			class brakes_int_road {
				sound[] = {"A3\Sounds_F\vehicles\soft\noises\slipping_tires_loop_04_int", 0.354813, 1};
				frequency = 1;
				volume = "engineOn*asphalt*(1-camPos)*(LongSlipDrive factor[-0.02, -0.05])*(Speed factor[2, 11])";
			};

			class turn_left_int_road {
				sound[] = {"A3\Sounds_F\vehicles\soft\noises\slipping_tires_loop_02_int", db-10, 1};
				frequency = 1;
				volume = "engineOn*asphalt*(1-camPos)*(latSlipDrive factor[0, 0.1])*(Speed factor[0, 10])";
			};

			class turn_right_int_road {
				sound[] = {"A3\Sounds_F\vehicles\soft\noises\slipping_tires_loop_02_int", db-10, 1};
				frequency = 1;
				volume = "engineOn*asphalt*(1-camPos)*(latSlipDrive factor[0, -0.1])*(Speed factor[0, 10])";
			};

			class Waternoise_ext {
				sound[] = {"A3\Sounds_F\vehicles\noises\soft_driving_in_water", db-3, 1, 300};
				frequency = "1";
				volume = "(speed factor[0, 5]) * water * camPos + (speed factor[-0.1, -5]) * water * camPos";
			};

			class Waternoise_int {
				sound[] = {"A3\Sounds_F\vehicles\noises\soft_driving_in_water_int", 0.562341, 1, 100};
				frequency = "1";
				volume = "(speed factor[0, 5]) * water * (1-camPos) + (speed factor[-0.1, -5]) * water * (1-camPos)";
			};

			class RainExt {
				sound[] = {"A3\Sounds_F\vehicles\noises\rain1_ext", db0, 1.0, 100};
				frequency = 1;
				volume = "camPos * (rain - rotorSpeed/2) * 2";
			};

			class RainInt {
				sound[] = {"A3\Sounds_F\vehicles\noises\rain1_int", db0, 1.0, 100};
				frequency = 1;
				volume = "(1-camPos)*(rain - rotorSpeed/2)*2";
			};
		};

		wheelCircumference = 3.805;
		thrustDelay = 0.4;
		brakeIdleSpeed = 1.78;
		maxSpeed = 110;	// max speed on level road, km/h
		fuelCapacity = 61;
		antiRollbarForceCoef = 12.0;
		antiRollbarForceLimit = 5.0;
		antiRollbarSpeedMin = 10;
		antiRollbarSpeedMax = 110;
		idleRpm = 600;
		redRpm = 2500;

		class complexGearbox {
			GearboxRatios[] = {"R1", -5.03, "N", 0, "D1", 3.49, "D2", 1.96, "D3", 1.41, "D4", 1.0, "D5", 0.75, "D6", 0.55};
			TransmissionRatios[] = {"High", 8};
			gearBoxMode = "auto";
			moveOffGear = 1;
			driveString = "D";
			neutralString = "N";
			reverseString = "R";
		};
		simulation = "carx";
		dampersBumpCoef = 6.0;
		differentialType = "all_limited";
		frontRearSplit = 0.5;
		frontBias = 1.3;
		rearBias = 1.3;
		centreBias = 1.3;
		clutchStrength = 55.0;
		enginePower = 368;
		maxOmega = 262;
		peakTorque = 1050;
		dampingRateFullThrottle = 0.08;
		dampingRateZeroThrottleClutchEngaged = 2.0;
		dampingRateZeroThrottleClutchDisengaged = 0.35;
		torqueCurve[] = {{0.0, 0.2}, {0.278, 0.5}, {0.35, 0.85}, {0.461, 1.0}, {0.7, 0.95}, {0.75, 0.85}, {0.8, 0.7}, {1.0, 0.5}};
		switchTime = 0.51;

		class Wheels {
			class L1 {
				boneName = "wheel_1_1_damper";
				steering = 1;
				side = "left";
				center = "wheel_1_1_axis";
				boundary = "wheel_1_1_bound";
				width = 0.2;
				mass = 80;
				MOI = 40;
				dampingRate = 0.1;
				dampingRateDamaged = 1.0;
				dampingRateDestroyed = 1000.0;
				maxBrakeTorque = 10000;
				maxHandBrakeTorque = 0;
				suspTravelDirection[] = {0, -1, 0};
				suspForceAppPointOffset = "wheel_1_1_axis";
				tireForceAppPointOffset = "wheel_1_1_axis";
				maxCompression = 0.18;
				maxDroop = 0.15;
				sprungMass = 1615;
				springStrength = 40375;
				springDamperRate = 6460;
				longitudinalStiffnessPerUnitGravity = 10000;
				latStiffX = 25;
				latStiffY = 180;
				frictionVsSlipGraph[] = {{0, 1}, {0.5, 1}, {1, 1}};
			};

			class L2 : L1 {
				boneName = "wheel_1_2_damper";
				steering = 1;
				center = "wheel_1_2_axis";
				boundary = "wheel_1_2_bound";
				suspForceAppPointOffset = "wheel_1_2_axis";
				tireForceAppPointOffset = "wheel_1_2_axis";
			};

			class L3 : L1 {
				boneName = "wheel_1_3_damper";
				steering = 0;
				center = "wheel_1_3_axis";
				boundary = "wheel_1_3_bound";
				suspForceAppPointOffset = "wheel_1_3_axis";
				tireForceAppPointOffset = "wheel_1_3_axis";
				maxHandBrakeTorque = 300000;
			};

			class R1 : L1 {
				boneName = "wheel_2_1_damper";
				center = "wheel_2_1_axis";
				boundary = "wheel_2_1_bound";
				suspForceAppPointOffset = "wheel_2_1_axis";
				tireForceAppPointOffset = "wheel_2_1_axis";
				steering = 1;
				side = "right";
			};

			class R2 : R1 {
				boneName = "wheel_2_2_damper";
				steering = 1;
				center = "wheel_2_2_axis";
				boundary = "wheel_2_2_bound";
				suspForceAppPointOffset = "wheel_2_2_axis";
				tireForceAppPointOffset = "wheel_2_2_axis";
			};

			class R3 : R1 {
				boneName = "wheel_2_3_damper";
				steering = 0;
				center = "wheel_2_3_axis";
				boundary = "wheel_2_3_bound";
				suspForceAppPointOffset = "wheel_2_3_axis";
				tireForceAppPointOffset = "wheel_2_3_axis";
				maxHandBrakeTorque = 300000;
			};

		};

		class RenderTargets {
			class Gunner_display {
				renderTarget = "rendertarget0";

				class CameraView1 {
					pointPosition = "PIP0_pos";
					pointDirection = "PIP0_dir";
					renderVisionMode = 2;
					renderQuality = 2;
					fov = 0.35;
				};
			};
			class mirrorL {
				renderTarget = "rendertarget1";

				class mirror {
					pointPosition = "PIP1_pos";
					pointDirection = "PIP1_dir";
					renderVisionMode = 4;
					renderQuality = 2;
					fov = 0.5;
				};
			};
			class mirrorP {
				renderTarget = "rendertarget2";

				class mirror {
					pointPosition = "PIP2_pos";
					pointDirection = "PIP2_dir";
					renderVisionMode = 4;
					renderQuality = 2;
					fov = 0.5;
				};
			};
			class mirrorC {
				renderTarget = "rendertarget3";

				class mirror {
					pointPosition = "PIP3_pos";
					pointDirection = "PIP3_dir";
					renderVisionMode = 4;
					renderQuality = 2;
					fov = 0.5;
				};
			};
			class Driver_display1 {
				renderTarget = "rendertarget4";

				class CameraView1 {
					pointPosition = "PIP4_pos";
					pointDirection = "PIP4_dir";
					renderVisionMode = 0;
					renderQuality = 2;
					fov = 0.7;
				};
			};
			class Driver_display2 {
				renderTarget = "rendertarget5";

				class CameraView1 {
					pointPosition = "PIP5_pos";
					pointDirection = "PIP5_dir";
					renderVisionMode = 0;
					renderQuality = 2;
					fov = 0.7;
				};
			};
			class Driver_display3 {
				renderTarget = "rendertarget6";

				class CameraView1 {
					pointPosition = "PIP6_pos";
					pointDirection = "PIP6_dir";
					renderVisionMode = 0;
					renderQuality = 2;
					fov = 0.7;
				};
			};
		};

		hideProxyInCombat = 1;
		viewDriverInExternal = 1;
		class CargoTurret;	// External class reference

		class Turrets : Turrets {
			class CommanderTurret : MainTurret {
				gunnerName = $STR_POSITION_COMMANDER;
				proxyType = "CPCommander";
				commanding = 2;
				primaryGunner = 0;
				primaryObserver = 1;
				gunnerCompartments = "Compartment2";
				body = "obsTurret";
				gun = "obsGun";
				animationSourceBody = "obsTurret";
				animationSourceGun = "obsGun";
				gunBeg = "optika_velitel_dir";	// endpoint of the gun
				gunEnd = "optika_velitel";	// chamber of the gun
				memoryPointGunnerOptics = "optika_velitel";
				weapons[] = {"Laserdesignator_mounted"};
				magazines[] = {"Laserbatteries"};
				gunnerAction = "vehicle_turnout_2";
				usePip = 0;
				gunnerOpticsModel = "\A3\Weapons_F_Beta\Reticle\Optics_Commander_01_F.p3d";
				Laser = true;
				turretInfoType = "RscOptics_Strider_commander";
				stabilizedInAxes = 0;
				canHideGunner = 1;
				forceHideGunner = false;
				gunnerForceOptics = false;
				inGunnerMayFire = true;
				outGunnerMayFire = false;
				startEngine = false;
				viewGunnerInExternal = true;
				minElev = -18;
				maxElev = 40;
				minTurn = -75;
				maxTurn = 75;
				isPersonTurret = 1;
				ejectDeadGunner = 0;
				animationSourceHatch = "hatch_3";
				enableManualFire = 0;
				memoryPointsGetInGunner = "pos commander";
				memoryPointsGetInGunnerDir = "pos commander dir";
				gunnerInAction = "passenger_low01";
				personTurretAction = "vehicle_turnout_2";
				gunnerGetInAction = "GetInLow";
				gunnerGetOutAction = "GetOutLow";
				gunnerDoor = "door_rear";
				LODTurnedIn = -1;
				LODTurnedOut = 1;
				class TurnIn {
					turnOffset = -180;
					};
				class TurnOut: TurnIn {};

				class ViewOptics : ViewOptics {
					initAngleX = 0;
					minAngleX = -30;
					maxAngleX = 30;
					initAngleY = 0;
					minAngleY = -100;
					maxAngleY = 100;
					initFov = 0.755;
					minFov = 0.034;
					maxFov = 0.9;
					visionMode[] = {"Normal", NVG, "Ti"};
					thermalMode[] = {2, 3};
				};

				class ViewGunner : ViewOptics {
					initAngleX = -15;
					minAngleX = -45;
					maxAngleX = 45;
					minFov = 0.25;
					maxFov = 1.25;
					initFov = 0.75;
					visionMode[] = {};
				};
				class HitPoints {
					class HitTurret {
						armor = 0.2;
						material = -1;
						name = "commander_turret_hit";
						visual = "commander_turret_hit";
						passThrough = false;
						minimalHit = 0.1;
						explosionShielding = 1;
						radius = 0.25;
					};

					class HitGun {
						armor = 0.2;
						material = -1;
						name = "commander_turret_hit";
						visual = "commander_turret_hit";
						passThrough = false;
						minimalHit = 0.1;
						explosionShielding = 1;
						radius = 0.25;
					};
				};
			};
			class CargoTurret_01 : CargoTurret {
				gunnerAction = "vehicle_turnout_1";
				gunnerCompartments = "Compartment2";
				memoryPointsGetInGunner = "pos cargoffv1";
				memoryPointsGetInGunnerDir = "pos cargoffv1 dir";
				gunnerName = "Passenger (left rear)";
				proxyIndex = 10;
				maxElev = 15;
				minElev = -42;
				maxTurn = 20;
				minTurn = -95;
				isPersonTurret = 1;
				ejectDeadGunner = 0;
				gunnerInAction = "passenger_apc_generic02";
				startEngine = 0;
				commanding = -1;
				outGunnerMayFire = true;
				inGunnerMayFire = false;
				animationSourceHatch = "hatch_1";
				gunnerGetInAction = "GetInLow";
				gunnerGetOutAction = "GetOutLow";
				memoryPointGunnerOptics = "";
				selectionFireAnim = "";
				canHideGunner = 1;
				gunnerForceOptics = false;
				gunnerDoor = "door_rear";
				LODTurnedIn = -1;
				LODTurnedOut = 1;
				class TurnIn {
					turnOffset = -180;
					};
				class TurnOut: TurnIn {};
			};
			class CargoTurret_02 : CargoTurret_01 {
				gunnerAction = "vehicle_turnout_2";
				memoryPointsGetInGunner = "pos cargoffv2";
				memoryPointsGetInGunnerDir = "pos cargoffv2 dir";
				gunnerName = "Passenger (right rear)";
				proxyIndex = 9;
				gunnerInAction = "passenger_apc_narrow_generic02";
				animationSourceHatch = "hatch_2";
			};
			class CargoTurret_03 : CargoTurret_01 {
				gunnerAction = "vehicle_turnout_2";
				memoryPointsGetInGunner = "pos codriver";
				memoryPointsGetInGunnerDir = "pos codriver dir";
				gunnerName = "Co-Driver";
				proxyIndex = 11;
				gunnerInAction = "passenger_low01";
				animationSourceHatch = "hatch_4";
				gunnerDoor = "door_rf";
			};
		};
		extCameraPosition[] = {0, 3, -15};

		class Damage {
			tex[] = {};
			mat[] = {"qin_titus\data\karoserie1.rvmat", "qin_titus\data\karoserie1_damage.rvmat", "qin_titus\data\karoserie1_destruct.rvmat", "qin_titus\data\karoserie2.rvmat", "qin_titus\data\karoserie2_damage.rvmat", "qin_titus\data\karoserie2_destruct.rvmat", "qin_titus\data\karoserie3.rvmat", "qin_titus\data\karoserie3_damage.rvmat", "qin_titus\data\karoserie3_destruct.rvmat", "qin_titus\data\arx20.rvmat", "qin_titus\data\arx20_damage.rvmat", "qin_titus\data\arx20_destruct.rvmat", "qin_titus\data\interier1.rvmat", "qin_titus\data\interier1_damage.rvmat", "qin_titus\data\interier1_destruct.rvmat", "qin_titus\data\interier1_kov.rvmat", "qin_titus\data\interier1_kov_damage.rvmat", "qin_titus\data\interier1_destruct.rvmat", "qin_titus\data\interier2.rvmat", "qin_titus\data\interier2_damage.rvmat", "qin_titus\data\interier2_destruct.rvmat", "qin_titus\data\interier2_kov.rvmat", "qin_titus\data\interier2_kov_damage.rvmat", "qin_titus\data\interier2_destruct.rvmat", "qin_titus\data\m240.rvmat", "qin_titus\data\m240_damage.rvmat", "qin_titus\data\m240_destruct.rvmat", "qin_titus\data\palubka.rvmat", "qin_titus\data\palubka_damage.rvmat", "qin_titus\data\palubka_destruct.rvmat", "qin_titus\data\pist.rvmat", "qin_titus\data\pist_damage.rvmat", "qin_titus\data\pist_destruct.rvmat", "qin_titus\data\podvozek.rvmat", "qin_titus\data\podvozek_damage.rvmat", "qin_titus\data\podvozek_destruct.rvmat", "qin_titus\data\svetla.rvmat", "qin_titus\data\svetla_damage.rvmat", "qin_titus\data\svetla_destruct.rvmat", "qin_titus\data\sedadla.rvmat", "qin_titus\data\sedadla.rvmat", "qin_titus\data\sedadla_destruct.rvmat", "qin_titus\data\volant.rvmat", "qin_titus\data\volant.rvmat", "qin_titus\data\volant_destruct.rvmat", "qin_titus\data\zrcatka.rvmat", "qin_titus\data\zrcatka.rvmat", "qin_titus\data\zrcatka_destruct.rvmat", "qin_titus\data\hasicak.rvmat", "qin_titus\data\hasicak_damage.rvmat", "qin_titus\data\hasicak_destruct.rvmat", "qin_titus\data\sklo.rvmat", "A3\data_f\Glass_veh_armored_damage.rvmat", "A3\data_f\Glass_veh_armored_damage.rvmat"};
		};

		class Reflectors {
			class Left {
				color[] = {1900, 1300, 950};
				ambient[] = {5, 5, 5};
				position = "Light_L";
				direction = "Light_L_end";
				hitpoint = "Light_L";
				selection = "Light_L";
				size = 1;
				innerAngle = 100;
				outerAngle = 179;
				coneFadeCoef = 10;
				intensity = 1;
				useFlare = 0;
				dayLight = 0;
				flareSize = 1.0;

				class Attenuation {
					start = 1.0;
					constant = 0;
					linear = 0;
					quadratic = 0.25;
					hardLimitStart = 30;
					hardLimitEnd = 60;
				};
			};

			class Right : Left {
				position = "Light_R";
				direction = "Light_R_end";
				hitpoint = "Light_R";
				selection = "Light_R";
			};

			class Right2 : Right {
				useFlare = 1;
				position = "light_R_flare";
			};

			class Left2 : Left {
				useFlare = 1;
				position = "light_L_flare";
			};
		};
		aggregateReflectors[] = {{"Left", "Right", "Left2", "Right2"}};
		hiddenSelections[] = {"Karoserie1", "Karoserie2", "Karoserie3", "ARX20", "Interier1", "Interier2", "Camonet", "display_gps", "display_gps_osy"};
		hiddenSelectionsTextures[] = {"qin_titus\data\Karoserie1_co.paa", "qin_titus\data\Karoserie2_co.paa", "qin_titus\data\Karoserie3_co.paa", "qin_titus\data\ARX20_co.paa", "qin_titus\data\Interier1_co.paa", "qin_titus\data\Interier2_co.paa", "qin_titus\data\camonet_co.paa", "qin_titus\data\gps\no_data.paa", "#(argb,8,8,3)color(0.86,0.14,0.02,0.35,ca)"};
		numberPhysicalWheels = 6;

		class TextureSources
		{
			class Desert
			{
				displayName = "Desert";
				author = "Qinetix";
				textures[] = {"qin_titus\data\karoserie1_co.paa", "qin_titus\data\karoserie2_co.paa", "qin_titus\data\karoserie3_co.paa", "qin_titus\data\arx20_co.paa", "qin_titus\data\interier1_co.paa", "qin_titus\data\interier2_co.paa", "qin_titus\data\camonet_co.paa"};
				faction[] = {"BLU_F"};
			};
			class Woodland
			{
				displayName = "Woodland";
				author = "Qinetix";
				textures[] = {"qin_titus\data\skins\wdl\karoserie1_wdl.paa", "qin_titus\data\skins\wdl\karoserie2_wdl.paa", "qin_titus\data\skins\wdl\karoserie3_wdl.paa", "qin_titus\data\skins\wdl\arx20_wdl.paa", "qin_titus\data\skins\wdl\interier1_wdl.paa", "qin_titus\data\skins\wdl\interier2_wdl.paa", "qin_titus\data\camonet_wdl_co.paa"};
				faction[] = {"BLU_F"};
			};
			class Woodland2
			{
				displayName = "Woodland 2";
				author = "Qinetix";
				textures[] = {"qin_titus\data\skins\wdl2\karoserie1_wdl2.paa", "qin_titus\data\skins\wdl2\karoserie2_wdl2.paa", "qin_titus\data\skins\wdl2\karoserie3_wdl2.paa", "qin_titus\data\skins\wdl2\arx20_wdl2.paa", "qin_titus\data\skins\wdl2\interier1_wdl2.paa", "qin_titus\data\skins\wdl2\interier2_wdl2.paa", "qin_titus\data\camonet_wdl_co.paa"};
				faction[] = {"BLU_F"};
			};
			class Camo_Desert
			{
				displayName = "Camo Desert";
				author = "Qinetix";
				textures[] = {"qin_titus\data\skins\camodes\karoserie1_camodes.paa", "qin_titus\data\skins\camodes\karoserie2_camodes.paa", "qin_titus\data\skins\camodes\karoserie3_camodes.paa", "qin_titus\data\skins\camodes\arx20_camodes.paa", "qin_titus\data\interier1_co.paa", "qin_titus\data\interier2_co.paa", "qin_titus\data\camonet_co.paa"};
				faction[] = {"BLU_F"};
			};
			class Black
			{
				displayName = "Black";
				author = "Qinetix";
				textures[] = {"qin_titus\data\skins\black\karoserie1_black.paa", "qin_titus\data\skins\black\karoserie2_black.paa", "qin_titus\data\skins\black\karoserie3_black.paa", "qin_titus\data\skins\black\arx20_black.paa", "qin_titus\data\skins\black\interier1_black.paa", "qin_titus\data\skins\black\interier2_black.paa", "qin_titus\data\camonet_wdl_co.paa"};
				faction[] = {"BLU_F"};
			};
		};

		class TransportBackpacks
		{
			class _xx_B_UAV_01_backpack_F
			{
				backpack = "B_UAV_01_backpack_F";
				count = 1;
			};
		};

		class Components;	// External class reference

		class AnimationSources : AnimationSources {
			class Door_LF {
				source = "door";
				animPeriod = 0.8;
				sound = "MetalDoorsSound";
				soundPosition = "door_1_axis";
			};

			class Door_RF {
				source = "door";
				animPeriod = 0.8;
				sound = "MetalDoorsSound";
				soundPosition = "door_2_axis";
			};

			class Door_rear {
				source = "door";
				animPeriod = 3;
				sound = "ServoDoorsSound";
				soundPosition = "door_3_axis";
			};

			class map_pos_x {
				source = "user";
				animPeriod = 0;
			};

			class map_pos_y {
				source = "user";
				animPeriod = 0;
			};

			class hatch_1 {
				source = "user";
				animPeriod = 0;
			};
			class hatch_2 {
				source = "user";
				animPeriod = 0;
			};
			class hatch_3 {
				source = "hatchCommander";
				animPeriod = 0;
			};
			class hatch_4 {
				source = "user";
				animPeriod = 0;
			};
			class comp_camonet {
				displayName = "$STR_A3_animationsources_showcamonethull0";
				author = "Qinetix";
				source = "user";
				animPeriod = 0.000001;
				initPhase = 0;
				mass = -50;
			};
			class comp_wind_grilles {
				displayName = "Show Windows Grilles";
				author = "Qinetix";
				source = "user";
				animPeriod = 0.000001;
				initPhase = 0;
				mass = -30;
			};
			class comp_klec {
				displayName = "$STR_A3_animationsources_showslathull0";
				author = "Qinetix";
				source = "user";
				animPeriod = 0.000001;
				initPhase = 0;
				mass = -150;
			};
			class comp_police {
				displayName = "Police parts";
				author = "Qinetix";
				source = "user";
				animPeriod = 0.000001;
				initPhase = 0;
				mass = 0;
			};
			class police_lights_l {
				source = "user";
				animPeriod = 0.000001;
			};
			class police_lights_r {
				source = "user";
				animPeriod = 0.000001;
			};
			class HitSLAT_left_1_src {
				source = "Hit";
				hitpoint = "HitSLAT_Left_1";
				raw = 1;
			};
			class HitSLAT_left_2_src {
				source = "Hit";
				hitpoint = "HitSLAT_Left_2";
				raw = 1;
			};
			class HitSLAT_left_3_src {
				source = "Hit";
				hitpoint = "HitSLAT_Left_3";
				raw = 1;
			};
			class HitSLAT_left_4_src {
				source = "Hit";
				hitpoint = "HitSLAT_Left_4";
				raw = 1;
			};
			class HitSLAT_left_5_src {
				source = "Hit";
				hitpoint = "HitSLAT_Left_5";
				raw = 1;
			};
			class HitSLAT_right_1_src {
				source = "Hit";
				hitpoint = "HitSLAT_Right_1";
				raw = 1;
			};
			class HitSLAT_right_2_src {
				source = "Hit";
				hitpoint = "HitSLAT_Right_2";
				raw = 1;
			};
			class HitSLAT_right_3_src {
				source = "Hit";
				hitpoint = "HitSLAT_Right_3";
				raw = 1;
			};
			class HitSLAT_right_4_src {
				source = "Hit";
				hitpoint = "HitSLAT_Right_4";
				raw = 1;
			};
			class HitSLAT_right_5_src {
				source = "Hit";
				hitpoint = "HitSLAT_Right_5";
				raw = 1;
			};
			class HitSLAT_front_1_src {
				source = "Hit";
				hitpoint = "HitSLAT_Front_1";
				raw = 1;
			};
			class HitSLAT_front_2_src {
				source = "Hit";
				hitpoint = "HitSLAT_Front_2";
				raw = 1;
			};
			class HitSLAT_rear_1_src {
				source = "Hit";
				hitpoint = "HitSLAT_Rear_1";
				raw = 1;
			};
			class HitSLAT_rear_2_src {
				source = "Hit";
				hitpoint = "HitSLAT_Rear_2";
				raw = 1;
			};
			class HitSLAT_rear_3_src {
				source = "Hit";
				hitpoint = "HitSLAT_Rear_3";
				raw = 1;
			};
		};

	class UserActions {
		class door1_open
		  {
		    displayName = "Open door";
		    priority = 0;
		    radius = 10;
		    position = "aimpoint";
		    showWindow = 0;
		    onlyForPlayer = 1;
		    condition = "(player == driver this) && (alive this) && (this doorPhase ""Door_LF"" == 0) && (speed this < 10)";
		    statement = "this animateDoor [""Door_LF"",1]";
			};
		class door1_close
		  {
		    displayName = "Close door";
		    priority = 0;
		    radius = 10;
		    position = "aimpoint";
		    showWindow = 0;
		    onlyForPlayer = 1;
		    condition = "(player == driver this) && (alive this) && (this doorPhase ""Door_LF"" == 1) && (speed this < 10)";
		    statement = "this animateDoor [""Door_LF"",0]";
			};
		class door2_open
		  {
		    displayName = "Open door";
		    priority = 0;
		    radius = 10;
		    position = "aimpoint";
		    showWindow = 0;
		    onlyForPlayer = 1;
		    condition = "(player == gunner this) && (alive this) && (this doorPhase ""Door_RF"" == 0) && (speed this < 10)";
		    statement = "this animateDoor [""Door_RF"",1]";
			};
		class door2_close
		  {
		    displayName = "Close door";
		    priority = 0;
		    radius = 10;
		    position = "aimpoint";
		    showWindow = 0;
		    onlyForPlayer = 1;
		    condition = "(player == gunner this) && (alive this) && (this doorPhase ""Door_RF"" == 1) && (speed this < 10)";
		    statement = "this animateDoor [""Door_RF"",0]";
			};
		class door3_open
		  {
		    displayName = "Open rear door";
		    priority = 0;
		    radius = 10;
		    position = "aimpoint";
		    showWindow = 0;
		    onlyForPlayer = 1;
		    condition = "(player == driver this) && (alive this) && (this doorPhase ""Door_rear"" == 0) && (speed this < 10)";
		    statement = "this animateDoor [""Door_rear"",1]";
			};
		class door3_close
		  {
		    displayName = "Close rear door";
		    priority = 0;
		    radius = 10;
		    position = "aimpoint";
		    showWindow = 0;
		    onlyForPlayer = 1;
		    condition = "(player == driver this) && (alive this) && (this doorPhase ""Door_rear"" == 1) && (speed this < 10)";
		    statement = "this animateDoor [""Door_rear"",0]";
			};
		};
	};

	class QIN_Titus_armed_base : QIN_Titus_base {
		author = "Qinetix";
		unitInfoType = "RscUnitInfoTank";
		driverCanSee = 4+8+2+32+16;
		gunnerCanSee = 4+2+8+32+16;
		maxFordingDepth = -0.6;
		cost = 600000;

		getInProxyOrder[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

		// threat (VSoft, VArmor, VAir), how threatening vehicle is to unit types
		threat[] = {1, 0.6, 0.3};

		class Components : Components {
			class VehicleSystemsDisplayManagerComponentLeft : DefaultVehicleSystemsDisplayManagerLeft {
				class Components : components {
					class VehiclePrimaryGunnerDisplay {
						componentType = "TransportFeedDisplayComponent";
						source = "PrimaryGunner";
					};
				};
			};

			class VehicleSystemsDisplayManagerComponentRight : DefaultVehicleSystemsDisplayManagerRight {
				class Components : components {
					class VehiclePrimaryGunnerDisplay {
						componentType = "TransportFeedDisplayComponent";
						source = "PrimaryGunner";
					};
				};
			};
		};

		class Turrets : Turrets {
			class CommanderTurret : CommanderTurret {};

			class CargoTurret_01 : CargoTurret_01 {};
			class CargoTurret_02 : CargoTurret_02 {};

			class ARX20Turret : MainTurret {
				body = "mainTurret";
				gun = "mainGun";
				gunBeg = "usti hlavne";	// endpoint of the gun
				gunEnd = "konec hlavne";	// chamber of the gun
				weapons[] = {"QIN_M240C", "QIN_10M621_20mm","SmokeLauncher"};
				magazines[] = {"QIN_139Rnd_20mm_shells", "QIN_300Rnd_762x51_Belt", "QIN_300Rnd_762x51_Belt", "SmokeLauncherMag"};
				soundServo[] = {"A3\Sounds_F\vehicles\soft\noises\servo_turret_MRAP01",0.562341,1,10};
				soundServoVertical[] = {"A3\Sounds_F\vehicles\soft\noises\servo_turret_MRAP01",0.562341,1,10};
				commanding = 1;
				minElev = -15;
				initElev = 0;
				maxElev = 45;
				gunnerCompartments = "Compartment2";
				enableManualFire = 0;
				memoryPointsGetInGunner = "pos codriver";
				memoryPointsGetInGunnerDir = "pos codriver dir";
				gunnerAction = "vehicle_turnout_1";
				gunnerInAction = "passenger_low01";
				gunnerGetInAction = "GetInLow";
				gunnerGetOutAction = "GetOutLow";
				gunnerDoor = "door_rf";
				canHideGunner = 1;
				forceHideGunner = false;
				gunnerForceOptics = false;
				inGunnerMayFire = true;
				outGunnerMayFire = false;
				startEngine = false;
				viewGunnerInExternal = true;
				castGunnerShadow = true;
				isPersonTurret = 1;
				ejectDeadGunner = 0;
				animationSourceHatch = "hatch_4";
				//enabledByAnimationSource  = "hatch_4";
				personTurretAction = "vehicle_turnout_2";
				stabilizedInAxes = 3;
				gunnerOpticsModel = "\A3\weapons_f\reticle\Optics_Gunner_02_F";
				discreteDistance[] = {100, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 1100, 1200, 1300, 1400, 1500};
				usePip = 1;
				discreteDistanceInitIndex = 2;
				turretInfoType = "RscOptics_crows";
				showCrewAim = 0;
				memoryPointGun="usti hlavne2";
				memoryPointGunnerOptics = "PIP0_pos";
				selectionFireAnim = "zasleh";
				class TurnIn {
					turnOffset = -180;
					};
				class TurnOut: TurnIn {};

				class ViewOptics : RCWSOptics {};

				class ViewGunner : ViewOptics {
					initAngleX = -15;
					minAngleX = -45;
					maxAngleX = 45;
					minFov = 0.25;
					maxFov = 1.25;
					initFov = 0.75;
					visionMode[] = {"Normal", "NVG", "TI"};
				};

				class HitPoints {
					class HitTurret {
						armor = 0.8;
						material = -1;
						name = "vez";
						visual = "vez";
						passThrough = false;
						minimalHit = 0.1;
						explosionShielding = 0.4;
						radius = 0.6;
					};

					class HitGun {
						armor = 0.4;
						material = -1;
						name = "zbran";
						visual = "zbran";
						passThrough = false;
						minimalHit = 0.1;
						explosionShielding = 0.2;
						radius = 0.6;
					};
				};

				class MFD {
					class MFD_1 {
						topLeft = MFD_1_TL;
						topRight = MFD_1_TR;
						bottomLeft = MFD_1_BL;
						borderLeft = 0;
						borderRight = 0;
						borderTop = 0;
						borderBottom = 0;
						color[] = {0.84, 0.86, 0.84};
						alpha = 1;
						enableParallax = 0;
						font = "RobotoCondensedLight";

						class Bones {};

						class Draw {
							color[] = {1, 0, 0, 1};
							alpha = 0.5;

							class Driver_Heading {
								type = "text";
								source = "weaponHeading";
								sourceScale = 1;
								sourceLength = 3;
								scale = 1;
								align = "center";
								refreshRate = 0.08;
								pos[] = {{0.18, 0.89}, 1};
								right[] = {{0.25, 0.89}, 1};
								down[] = {{0.18, 0.96}, 1};
							};

							class Range_Text {
								type = "text";
								source = "static";
								text = "RANGE:";
								scale = 1;
								sourceScale = 1;
								align = "right";
								pos[] = {{0.515, 0.69}, 1};
								right[] = {{0.555, 0.69}, 1};
								down[] = {{0.515, 0.73}, 1};
							};

							class Range_Value {
								type = "text";
								source = "laserDist";
								sourceScale = 1;
								sourceLength = 3;
								scale = 1;
								align = "right";
								refreshRate = 0.08;
								pos[] = {{0.62, 0.69}, 1};
								right[] = {{0.66, 0.69}, 1};
								down[] = {{0.62, 0.73}, 1};
							};

							class Mode_Text {
								type = "text";
								source = "static";
								text = "MODE:";
								scale = 1;
								sourceScale = 1;
								align = "right";
								pos[] = {{0.515, 0.73}, 1};
								right[] = {{0.555, 0.73}, 1};
								down[] = {{0.515, 0.77}, 1};
							};

							class Mode_Value {
								type = "text";
								source = "static";
								text = "TI WHOT";
								sourceScale = 1;
								sourceLength = 3;
								scale = 1;
								align = "right";
								refreshRate = 0.08;
								pos[] = {{0.62, 0.73}, 1};
								right[] = {{0.66, 0.73}, 1};
								down[] = {{0.62, 0.77}, 1};
							};
						};
					};
				};
			};
		};

		smokeLauncherGrenadeCount = 4; //Number of smoke shells launched at once
		smokeLauncherVelocity = 14; //Velocity which smoke shells are launched at
		smokeLauncherOnTurret = 1; //0 if smoke launchers are on hull, 1 if on turret
		smokeLauncherAngle = 80; //Angle within which smoke grenades are launched (actual spacing may end up smaller so use slighty higher number)

		class AnimationSources : AnimationSources {
			class muzzle_rot {
				source = "ammorandom";
				weapon = "QIN_10M621_20mm";
			};
			class muzzle2_rot {
				source = "ammorandom";
				weapon = "QIN_M240C";
			};
			class muzzle_hide {
				source = "reload";
				weapon = "QIN_10M621_20mm";
			};
			class muzzle2_hide {
				source = "reload";
				weapon = "QIN_M240C";
			};
		};
	};
	class QIN_Titus_DES : QIN_Titus_base {
		author = "Qinetix";
		mapSize = 7.72;

		class SimpleObject {
			eden = 1;
			animate[] = {{"damagehide", 0}, {"damagehidevez", 0}, {"damagehidehlaven", 0}, {"wheel_1_1_destruct", 0}, {"wheel_1_2_destruct", 0}, {"wheel_1_3_destruct", 0}, {"wheel_1_4_destruct", 0}, {"wheel_2_1_destruct", 0}, {"wheel_2_2_destruct", 0}, {"wheel_2_3_destruct", 0}, {"wheel_2_4_destruct", 0}, {"wheel_1_1_destruct_unhide", 0}, {"wheel_1_2_destruct_unhide", 0}, {"wheel_1_3_destruct_unhide", 0}, {"wheel_1_4_destruct_unhide", 0}, {"wheel_2_1_destruct_unhide", 0}, {"wheel_2_2_destruct_unhide", 0}, {"wheel_2_3_destruct_unhide", 0}, {"wheel_2_4_destruct_unhide", 0}, {"wheel_1_3_damage", 0}, {"wheel_1_4_damage", 0}, {"wheel_2_3_damage", 0}, {"wheel_2_4_damage", 0}, {"wheel_1_3_damper_damage_backanim", 0}, {"wheel_1_4_damper_damage_backanim", 0}, {"wheel_2_3_damper_damage_backanim", 0}, {"wheel_2_4_damper_damage_backanim", 0}, {"glass1_destruct", 0}, {"glass2_destruct", 0}, {"glass3_destruct", 0}, {"glass4_destruct", 0}, {"glass5_destruct", 0}, {"glass6_destruct", 0}, {"fuel", 1}, {"wheel_1_1", 1}, {"wheel_2_1", 1}, {"wheel_1_2", 1}, {"wheel_2_2", 1}, {"wheel_1_1_damper", 0.48}, {"wheel_2_1_damper", 0.53}, {"wheel_1_2_damper", 0.48}, {"wheel_2_2_damper", 0.61}, {"daylights", 0}, {"pedal_thrust", 0}, {"pedal_brake", 1}, {"wheel_1_1_damage", 0}, {"wheel_1_2_damage", 0}, {"wheel_2_1_damage", 0}, {"wheel_2_2_damage", 0}, {"wheel_1_1_damper_damage_backanim", 0}, {"wheel_1_2_damper_damage_backanim", 0}, {"wheel_2_1_damper_damage_backanim", 0}, {"wheel_2_2_damper_damage_backanim", 0}, {"vehicletransported_antenna_hide", 0}, {"drivingwheel", 0}, {"steering_1_1", 0}, {"steering_2_1", 0}, {"indicatorspeed", 0}, {"indicatorfuel", 1}, {"indicatorrpm", 0}, {"indicatortemp", 0}, {"indicatortemp_move", 0}, {"indicatortemp2", 0}, {"indicatortemp2_move", 0}, {"reverse_light", 0}, {"door_lf", 0}, {"door_rf", 0}, {"door_lb", 0}, {"door_rb", 0}, {"mainturret", 0}, {"maingun", 0}, {"damagehlaven", 0}, {"zasleh_rot", 0}, {"zasleh2_rot", 4188.1}, {"zasleh_hide", 0}, {"hatch_1", 0}, {"hatch_2", 0}, {"hatch_3", 0}, {"hatch_4", 0}, {"comp_camonet", 0}, {"comp_wind_grilles", 0}, {"comp_klec", 0}, {"comp_police", 0}, {"police_lights_l", 0}, {"police_lights_l", 0}};
			hide[] = {"clan", "zasleh", "zasleh2", "light_l", "light_r", "light_r2", "light_l2", "zadni svetlo", "brzdove svetlo", "podsvit pristroju", "poskozeni"};
			verticalOffset = 2.582;
			verticalOffsetWorld = -0.176;
			init = "''";
		};
		editorPreview = "qin_titus\data\preview\titus_unarmed_des.jpg";
		picture = "qin_titus\data\UI\titus_unarmed_ca.paa";
		Icon = "qin_titus\data\UI\map_titus_unarmed_ca.paa";
		//_generalMacro = "B_MRAP_01_gmg_F";
		scope = 2;
		displayName = "Titus";
		model = "qin_titus\titus_unarmed.p3d";
		crew = "B_Soldier_F";
		typicalCargo[] = {"B_Soldier_lite_F"};
		side = TWest;
		faction = BLU_F;

		class EventHandlers : EventHandlers {
            Init = "_this execVM '\qin_titus\scripts\gps_map_init.sqf'";
            GetIn = "_this execVM '\qin_titus\scripts\uav_terminal.sqf'";
         	};
		};
	class QIN_Titus_WDL : QIN_Titus_base {
		author = "Qinetix";
		mapSize = 7.72;

		class SimpleObject {
			eden = 1;
			animate[] = {{"damagehide", 0}, {"damagehidevez", 0}, {"damagehidehlaven", 0}, {"wheel_1_1_destruct", 0}, {"wheel_1_2_destruct", 0}, {"wheel_1_3_destruct", 0}, {"wheel_1_4_destruct", 0}, {"wheel_2_1_destruct", 0}, {"wheel_2_2_destruct", 0}, {"wheel_2_3_destruct", 0}, {"wheel_2_4_destruct", 0}, {"wheel_1_1_destruct_unhide", 0}, {"wheel_1_2_destruct_unhide", 0}, {"wheel_1_3_destruct_unhide", 0}, {"wheel_1_4_destruct_unhide", 0}, {"wheel_2_1_destruct_unhide", 0}, {"wheel_2_2_destruct_unhide", 0}, {"wheel_2_3_destruct_unhide", 0}, {"wheel_2_4_destruct_unhide", 0}, {"wheel_1_3_damage", 0}, {"wheel_1_4_damage", 0}, {"wheel_2_3_damage", 0}, {"wheel_2_4_damage", 0}, {"wheel_1_3_damper_damage_backanim", 0}, {"wheel_1_4_damper_damage_backanim", 0}, {"wheel_2_3_damper_damage_backanim", 0}, {"wheel_2_4_damper_damage_backanim", 0}, {"glass1_destruct", 0}, {"glass2_destruct", 0}, {"glass3_destruct", 0}, {"glass4_destruct", 0}, {"glass5_destruct", 0}, {"glass6_destruct", 0}, {"fuel", 1}, {"wheel_1_1", 1}, {"wheel_2_1", 1}, {"wheel_1_2", 1}, {"wheel_2_2", 1}, {"wheel_1_1_damper", 0.48}, {"wheel_2_1_damper", 0.53}, {"wheel_1_2_damper", 0.48}, {"wheel_2_2_damper", 0.61}, {"daylights", 0}, {"pedal_thrust", 0}, {"pedal_brake", 1}, {"wheel_1_1_damage", 0}, {"wheel_1_2_damage", 0}, {"wheel_2_1_damage", 0}, {"wheel_2_2_damage", 0}, {"wheel_1_1_damper_damage_backanim", 0}, {"wheel_1_2_damper_damage_backanim", 0}, {"wheel_2_1_damper_damage_backanim", 0}, {"wheel_2_2_damper_damage_backanim", 0}, {"vehicletransported_antenna_hide", 0}, {"drivingwheel", 0}, {"steering_1_1", 0}, {"steering_2_1", 0}, {"indicatorspeed", 0}, {"indicatorfuel", 1}, {"indicatorrpm", 0}, {"indicatortemp", 0}, {"indicatortemp_move", 0}, {"indicatortemp2", 0}, {"indicatortemp2_move", 0}, {"reverse_light", 0}, {"door_lf", 0}, {"door_rf", 0}, {"door_lb", 0}, {"door_rb", 0}, {"mainturret", 0}, {"maingun", 0}, {"damagehlaven", 0}, {"zasleh_rot", 0}, {"zasleh2_rot", 4188.1}, {"zasleh_hide", 0}, {"hatch_1", 0}, {"hatch_2", 0}, {"hatch_3", 0}, {"hatch_4", 0}, {"comp_camonet", 0}, {"comp_wind_grilles", 0}, {"comp_klec", 0}, {"comp_police", 0}, {"police_lights_l", 0}, {"police_lights_l", 0}};
			hide[] = {"clan", "zasleh", "zasleh2", "light_l", "light_r", "light_r2", "light_l2", "zadni svetlo", "brzdove svetlo", "podsvit pristroju", "poskozeni"};
			verticalOffset = 2.582;
			verticalOffsetWorld = -0.176;
			init = "''";
		};
		editorPreview = "qin_titus\data\preview\titus_unarmed_wdl.jpg";
		picture = "qin_titus\data\UI\titus_unarmed_ca.paa";
		Icon = "qin_titus\data\UI\map_titus_unarmed_ca.paa";
		//_generalMacro = "B_MRAP_01_gmg_F";
		scope = 2;
		displayName = "Titus";
		model = "qin_titus\titus_unarmed.p3d";
		crew = "B_T_Soldier_F";
		typicalCargo[] = {"B_Soldier_lite_F"};
		side = TWest;
		faction = BLU_T_F;

		hiddenSelectionsTextures[] = {"qin_titus\data\skins\wdl\karoserie1_wdl.paa", "qin_titus\data\skins\wdl\karoserie2_wdl.paa", "qin_titus\data\skins\wdl\karoserie3_wdl.paa", "qin_titus\data\skins\wdl\arx20_wdl.paa", "qin_titus\data\skins\wdl\interier1_wdl.paa", "qin_titus\data\skins\wdl\interier2_wdl.paa", "qin_titus\data\camonet_wdl_co.paa", "qin_titus\data\gps\no_data.paa", "#(argb,8,8,3)color(0.86,0.14,0.02,0.35,ca)"};

		class EventHandlers : EventHandlers {
            Init = "_this execVM '\qin_titus\scripts\gps_map_init.sqf'";
            GetIn = "_this execVM '\qin_titus\scripts\uav_terminal.sqf'";
         	};
		};
	class QIN_Titus_arx20_DES : QIN_Titus_armed_base {
		author = "Qinetix";
		mapSize = 7.72;

		class SimpleObject {
			eden = 1;
			animate[] = {{"damagehide", 0}, {"damagehidevez", 0}, {"damagehidehlaven", 0}, {"wheel_1_1_destruct", 0}, {"wheel_1_2_destruct", 0}, {"wheel_1_3_destruct", 0}, {"wheel_1_4_destruct", 0}, {"wheel_2_1_destruct", 0}, {"wheel_2_2_destruct", 0}, {"wheel_2_3_destruct", 0}, {"wheel_2_4_destruct", 0}, {"wheel_1_1_destruct_unhide", 0}, {"wheel_1_2_destruct_unhide", 0}, {"wheel_1_3_destruct_unhide", 0}, {"wheel_1_4_destruct_unhide", 0}, {"wheel_2_1_destruct_unhide", 0}, {"wheel_2_2_destruct_unhide", 0}, {"wheel_2_3_destruct_unhide", 0}, {"wheel_2_4_destruct_unhide", 0}, {"wheel_1_3_damage", 0}, {"wheel_1_4_damage", 0}, {"wheel_2_3_damage", 0}, {"wheel_2_4_damage", 0}, {"wheel_1_3_damper_damage_backanim", 0}, {"wheel_1_4_damper_damage_backanim", 0}, {"wheel_2_3_damper_damage_backanim", 0}, {"wheel_2_4_damper_damage_backanim", 0}, {"glass1_destruct", 0}, {"glass2_destruct", 0}, {"glass3_destruct", 0}, {"glass4_destruct", 0}, {"glass5_destruct", 0}, {"glass6_destruct", 0}, {"fuel", 1}, {"wheel_1_1", 1}, {"wheel_2_1", 1}, {"wheel_1_2", 1}, {"wheel_2_2", 1}, {"wheel_1_1_damper", 0.48}, {"wheel_2_1_damper", 0.53}, {"wheel_1_2_damper", 0.48}, {"wheel_2_2_damper", 0.61}, {"daylights", 0}, {"pedal_thrust", 0}, {"pedal_brake", 1}, {"wheel_1_1_damage", 0}, {"wheel_1_2_damage", 0}, {"wheel_2_1_damage", 0}, {"wheel_2_2_damage", 0}, {"wheel_1_1_damper_damage_backanim", 0}, {"wheel_1_2_damper_damage_backanim", 0}, {"wheel_2_1_damper_damage_backanim", 0}, {"wheel_2_2_damper_damage_backanim", 0}, {"vehicletransported_antenna_hide", 0}, {"drivingwheel", 0}, {"steering_1_1", 0}, {"steering_2_1", 0}, {"indicatorspeed", 0}, {"indicatorfuel", 1}, {"indicatorrpm", 0}, {"indicatortemp", 0}, {"indicatortemp_move", 0}, {"indicatortemp2", 0}, {"indicatortemp2_move", 0}, {"reverse_light", 0}, {"door_lf", 0}, {"door_rf", 0}, {"door_lb", 0}, {"door_rb", 0}, {"mainturret", 0}, {"maingun", 0}, {"damagehlaven", 0}, {"zasleh_rot", 0}, {"zasleh2_rot", 4188.1}, {"zasleh_hide", 0}, {"hatch_1", 0}, {"hatch_2", 0}, {"hatch_3", 0}, {"hatch_4", 0}, {"comp_camonet", 0}, {"comp_wind_grilles", 0}, {"comp_klec", 0}, {"comp_police", 0}, {"police_lights_l", 0}, {"police_lights_l", 0}};
			hide[] = {"clan", "zasleh", "zasleh2", "light_l", "light_r", "light_r2", "light_l2", "zadni svetlo", "brzdove svetlo", "podsvit pristroju", "poskozeni"};
			verticalOffset = 2.582;
			verticalOffsetWorld = -0.176;
			init = "''";
		};
		editorPreview = "qin_titus\data\preview\titus_arx20_des.jpg";
		picture = "qin_titus\data\UI\titus_arx20_ca.paa";
		Icon = "qin_titus\data\UI\map_titus_arx20_ca.paa";
		//_generalMacro = "B_MRAP_01_gmg_F";
		scope = 2;
		displayName = "Titus ARX20";
		model = "qin_titus\titus_arx20.p3d";
		crew = "B_Soldier_F";
		typicalCargo[] = {"B_Soldier_lite_F"};
		side = TWest;
		faction = BLU_F;

		class EventHandlers : EventHandlers {
            Init = "_this execVM '\qin_titus\scripts\gps_map_init.sqf'";
            GetIn = "_this execVM '\qin_titus\scripts\uav_terminal.sqf'";
         	};
		};
	class QIN_Titus_arx20_WDL : QIN_Titus_armed_base {
		author = "Qinetix";
		mapSize = 7.72;

		class SimpleObject {
			eden = 1;
			animate[] = {{"damagehide", 0}, {"damagehidevez", 0}, {"damagehidehlaven", 0}, {"wheel_1_1_destruct", 0}, {"wheel_1_2_destruct", 0}, {"wheel_1_3_destruct", 0}, {"wheel_1_4_destruct", 0}, {"wheel_2_1_destruct", 0}, {"wheel_2_2_destruct", 0}, {"wheel_2_3_destruct", 0}, {"wheel_2_4_destruct", 0}, {"wheel_1_1_destruct_unhide", 0}, {"wheel_1_2_destruct_unhide", 0}, {"wheel_1_3_destruct_unhide", 0}, {"wheel_1_4_destruct_unhide", 0}, {"wheel_2_1_destruct_unhide", 0}, {"wheel_2_2_destruct_unhide", 0}, {"wheel_2_3_destruct_unhide", 0}, {"wheel_2_4_destruct_unhide", 0}, {"wheel_1_3_damage", 0}, {"wheel_1_4_damage", 0}, {"wheel_2_3_damage", 0}, {"wheel_2_4_damage", 0}, {"wheel_1_3_damper_damage_backanim", 0}, {"wheel_1_4_damper_damage_backanim", 0}, {"wheel_2_3_damper_damage_backanim", 0}, {"wheel_2_4_damper_damage_backanim", 0}, {"glass1_destruct", 0}, {"glass2_destruct", 0}, {"glass3_destruct", 0}, {"glass4_destruct", 0}, {"glass5_destruct", 0}, {"glass6_destruct", 0}, {"fuel", 1}, {"wheel_1_1", 1}, {"wheel_2_1", 1}, {"wheel_1_2", 1}, {"wheel_2_2", 1}, {"wheel_1_1_damper", 0.48}, {"wheel_2_1_damper", 0.53}, {"wheel_1_2_damper", 0.48}, {"wheel_2_2_damper", 0.61}, {"daylights", 0}, {"pedal_thrust", 0}, {"pedal_brake", 1}, {"wheel_1_1_damage", 0}, {"wheel_1_2_damage", 0}, {"wheel_2_1_damage", 0}, {"wheel_2_2_damage", 0}, {"wheel_1_1_damper_damage_backanim", 0}, {"wheel_1_2_damper_damage_backanim", 0}, {"wheel_2_1_damper_damage_backanim", 0}, {"wheel_2_2_damper_damage_backanim", 0}, {"vehicletransported_antenna_hide", 0}, {"drivingwheel", 0}, {"steering_1_1", 0}, {"steering_2_1", 0}, {"indicatorspeed", 0}, {"indicatorfuel", 1}, {"indicatorrpm", 0}, {"indicatortemp", 0}, {"indicatortemp_move", 0}, {"indicatortemp2", 0}, {"indicatortemp2_move", 0}, {"reverse_light", 0}, {"door_lf", 0}, {"door_rf", 0}, {"door_lb", 0}, {"door_rb", 0}, {"mainturret", 0}, {"maingun", 0}, {"damagehlaven", 0}, {"zasleh_rot", 0}, {"zasleh2_rot", 4188.1}, {"zasleh_hide", 0}, {"hatch_1", 0}, {"hatch_2", 0}, {"hatch_3", 0}, {"hatch_4", 0}, {"comp_camonet", 0}, {"comp_wind_grilles", 0}, {"comp_klec", 0}, {"comp_police", 0}, {"police_lights_l", 0}, {"police_lights_l", 0}};
			hide[] = {"clan", "zasleh", "zasleh2", "light_l", "light_r", "light_r2", "light_l2", "zadni svetlo", "brzdove svetlo", "podsvit pristroju", "poskozeni"};
			verticalOffset = 2.582;
			verticalOffsetWorld = -0.176;
			init = "''";
		};
		editorPreview = "qin_titus\data\preview\titus_arx20_wdl.jpg";
		picture = "qin_titus\data\UI\titus_arx20_ca.paa";
		Icon = "qin_titus\data\UI\map_titus_arx20_ca.paa";
		//_generalMacro = "B_MRAP_01_gmg_F";
		scope = 2;
		displayName = "Titus ARX20";
		model = "qin_titus\titus_arx20.p3d";
		crew = "B_T_Soldier_F";
		typicalCargo[] = {"B_Soldier_lite_F"};
		side = TWest;
		faction = BLU_T_F;

		hiddenSelectionsTextures[] = {"qin_titus\data\skins\wdl\karoserie1_wdl.paa", "qin_titus\data\skins\wdl\karoserie2_wdl.paa", "qin_titus\data\skins\wdl\karoserie3_wdl.paa", "qin_titus\data\skins\wdl\arx20_wdl.paa", "qin_titus\data\skins\wdl\interier1_wdl.paa", "qin_titus\data\skins\wdl\interier2_wdl.paa", "qin_titus\data\camonet_wdl_co.paa", "qin_titus\data\gps\no_data.paa", "#(argb,8,8,3)color(0.86,0.14,0.02,0.35,ca)"};

		class EventHandlers : EventHandlers {
            Init = "_this execVM '\qin_titus\scripts\gps_map_init.sqf'";
            GetIn = "_this execVM '\qin_titus\scripts\uav_terminal.sqf'";
         	};
		};
		class QIN_Titus_police : QIN_Titus_base {
		author = "Qinetix";
		mapSize = 7.72;

		class SimpleObject {
			eden = 1;
			animate[] = {{"damagehide", 0}, {"damagehidevez", 0}, {"damagehidehlaven", 0}, {"wheel_1_1_destruct", 0}, {"wheel_1_2_destruct", 0}, {"wheel_1_3_destruct", 0}, {"wheel_1_4_destruct", 0}, {"wheel_2_1_destruct", 0}, {"wheel_2_2_destruct", 0}, {"wheel_2_3_destruct", 0}, {"wheel_2_4_destruct", 0}, {"wheel_1_1_destruct_unhide", 0}, {"wheel_1_2_destruct_unhide", 0}, {"wheel_1_3_destruct_unhide", 0}, {"wheel_1_4_destruct_unhide", 0}, {"wheel_2_1_destruct_unhide", 0}, {"wheel_2_2_destruct_unhide", 0}, {"wheel_2_3_destruct_unhide", 0}, {"wheel_2_4_destruct_unhide", 0}, {"wheel_1_3_damage", 0}, {"wheel_1_4_damage", 0}, {"wheel_2_3_damage", 0}, {"wheel_2_4_damage", 0}, {"wheel_1_3_damper_damage_backanim", 0}, {"wheel_1_4_damper_damage_backanim", 0}, {"wheel_2_3_damper_damage_backanim", 0}, {"wheel_2_4_damper_damage_backanim", 0}, {"glass1_destruct", 0}, {"glass2_destruct", 0}, {"glass3_destruct", 0}, {"glass4_destruct", 0}, {"glass5_destruct", 0}, {"glass6_destruct", 0}, {"fuel", 1}, {"wheel_1_1", 1}, {"wheel_2_1", 1}, {"wheel_1_2", 1}, {"wheel_2_2", 1}, {"wheel_1_1_damper", 0.48}, {"wheel_2_1_damper", 0.53}, {"wheel_1_2_damper", 0.48}, {"wheel_2_2_damper", 0.61}, {"daylights", 0}, {"pedal_thrust", 0}, {"pedal_brake", 1}, {"wheel_1_1_damage", 0}, {"wheel_1_2_damage", 0}, {"wheel_2_1_damage", 0}, {"wheel_2_2_damage", 0}, {"wheel_1_1_damper_damage_backanim", 0}, {"wheel_1_2_damper_damage_backanim", 0}, {"wheel_2_1_damper_damage_backanim", 0}, {"wheel_2_2_damper_damage_backanim", 0}, {"vehicletransported_antenna_hide", 0}, {"drivingwheel", 0}, {"steering_1_1", 0}, {"steering_2_1", 0}, {"indicatorspeed", 0}, {"indicatorfuel", 1}, {"indicatorrpm", 0}, {"indicatortemp", 0}, {"indicatortemp_move", 0}, {"indicatortemp2", 0}, {"indicatortemp2_move", 0}, {"reverse_light", 0}, {"door_lf", 0}, {"door_rf", 0}, {"door_lb", 0}, {"door_rb", 0}, {"mainturret", 0}, {"maingun", 0}, {"damagehlaven", 0}, {"zasleh_rot", 0}, {"zasleh2_rot", 4188.1}, {"zasleh_hide", 0}, {"hatch_1", 0}, {"hatch_2", 0}, {"hatch_3", 0}, {"hatch_4", 0}, {"comp_camonet", 0}, {"comp_wind_grilles", 1}, {"comp_klec", 0}, {"comp_police", 1}, {"police_lights_l", 0}, {"police_lights_l", 0}};
			hide[] = {"clan", "zasleh", "zasleh2", "light_l", "light_r", "light_r2", "light_l2", "zadni svetlo", "brzdove svetlo", "podsvit pristroju", "poskozeni"};
			verticalOffset = 2.582;
			verticalOffsetWorld = -0.176;
			init = "''";
		};
		editorPreview = "qin_titus\data\preview\titus_unarmed_wdl.jpg";
		picture = "qin_titus\data\UI\titus_unarmed_ca.paa";
		Icon = "qin_titus\data\UI\map_titus_unarmed_ca.paa";
		//_generalMacro = "B_MRAP_01_gmg_F";
		scope = 2;
		displayName = "Titus (Police)";
		model = "qin_titus\titus_unarmed.p3d";
		crew = "C_man_1";
		typicalCargo[] = {"C_man_1"};
		side = TCivilian ;
		faction = CIV_F;

		hiddenSelectionsTextures[] = {"qin_titus\data\skins\black\karoserie1_black.paa", "qin_titus\data\skins\black\karoserie2_black.paa", "qin_titus\data\skins\black\karoserie3_black.paa", "qin_titus\data\skins\black\arx20_black.paa", "qin_titus\data\skins\black\interier1_black.paa", "qin_titus\data\skins\black\interier2_black.paa", "qin_titus\data\camonet_wdl_co.paa", "qin_titus\data\gps\no_data.paa", "#(argb,8,8,3)color(0.86,0.14,0.02,0.35,ca)"};

		class EventHandlers : EventHandlers {
            Init = "_this execVM '\qin_titus\scripts\police_init.sqf'";
            GetIn = "_this execVM '\qin_titus\scripts\uav_terminal.sqf'";
         	};

		class UserActions {
			class lightsOn {
				displayName = "<t color='#39e1ff'>Lights ON</t>";
				position = "mph_axis";
				onlyforplayer = "true";
				radius = 5;
				condition = "!(this getvariable [""lights_on"", false]) and (player == driver this)AND Alive(this) and canmove this";
				statement = "this setvariable [""lights_on"", true, true]";
			};

			class lightsOff {
				displayName = "<t color='#39e1ff'>Lights OFF</t>";
				position = "mph_axis";
				onlyforplayer = "true";
				radius = 5;
				condition = "(this getvariable [""lights_on"", false]) and (player == driver this)AND Alive(this) and canmove this";
				statement = "this setVariable [""lights_on"", false, true]";
			};

			class SirenOn {
				displayName = "<t color='#39e1ff'>Siren ON</t>";
				position = "mph_axis";
				onlyforplayer = "true";
				radius = 5;
				condition = "!(this getvariable [""siren_on"", false]) and (player == driver this)AND Alive(this) and canmove this";
				statement = "this setvariable [""siren_on"", true]";
			};

			class SirenOff {
				displayName = "<t color='#39e1ff'>Siren OFF</t>";
				position = "mph_axis";
				onlyforplayer = "true";
				radius = 5;
				condition = "(this getvariable [""siren_on"", false]) and (player == driver this)AND Alive(this) and canmove this";
				statement = "this setVariable [""siren_on"", false]";
			};
		};
	};
};