private ["_vehicle", "_majak"];

_vehicle = _this select 0;

[_vehicle] execVM "\qin_titus\scripts\gps_map_init.sqf";

svetla = compile preprocessFile "\qin_titus\scripts\svetla.sqf";

[_vehicle] spawn svetla;

[_vehicle] execVM "\qin_titus\scripts\sirena.sqf";