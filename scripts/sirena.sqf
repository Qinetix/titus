private ["_vehicle", "_sirena"];

_vehicle = _this select 0;

while {true} do {
		waitUntil {_vehicle getVariable "siren_on"};
		[[_vehicle,"qin_sirena"], "say3D",true,true,false] call BIS_fnC_MP;
		if (!canmove _vehicle) exitwith {};
		sleep 4.57;
};