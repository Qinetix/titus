_veh = _this select 0;

_showPos = compile preprocessFile "\qin_titus\scripts\gps_map_pos.sqf";

_map_name = worldName;
_map_size = [] call BIS_fnc_mapSize;

_map_loaded = 0;

if (_map_name == "Stratis" and _map_loaded == 0) then
	{
		_veh setObjectTextureGlobal [7, "qin_titus\data\gps\stratis.paa"];
		_map_loaded = 1;
	};
if (_map_name == "Altis" and _map_loaded == 0) then
	{
		_veh setObjectTextureGlobal [7, "qin_titus\data\gps\altis.paa"];
		_map_loaded = 1;
	};
if (_map_name == "Malden" and _map_loaded == 0) then
	{
		_veh setObjectTextureGlobal [7, "qin_titus\data\gps\malden2035.paa"];
		_map_loaded = 1;
		_map_size = 12800;
	};
if (_map_name == "Tanoa" and _map_loaded == 0) then
	{
		_veh setObjectTextureGlobal [7, "qin_titus\data\gps\tanoa.paa"];
		_map_loaded = 1;
	};
if (_map_name == "Chernarus" and _map_loaded == 0) then
	{
		_veh setObjectTextureGlobal [7, "qin_titus\data\gps\chernarus.paa"];
		_map_loaded = 1;
	};
if (_map_name == "Chernarus_Summer" and _map_loaded == 0) then
	{
		_veh setObjectTextureGlobal [7, "qin_titus\data\gps\chernarus.paa"];
		_map_loaded = 1;
	};
if (_map_name == "Chernarus_winter" and _map_loaded == 0) then
	{
		_veh setObjectTextureGlobal [7, "qin_titus\data\gps\chernarus_winter.paa"];
		_map_loaded = 1;
		_map_size = 15360;
	};
if (_map_name == "Takistan" and _map_loaded == 0) then
	{
		_veh setObjectTextureGlobal [7, "qin_titus\data\gps\takistan.paa"];
		_map_loaded = 1;
	};
if (_map_name == "pja310" and _map_loaded == 0) then
	{
		_veh setObjectTextureGlobal [7, "qin_titus\data\gps\al_rayak.paa"];
		_map_loaded = 1;
	};
if (_map_name == "avganiF" and _map_loaded == 0) then
	{
		_veh setObjectTextureGlobal [7, "qin_titus\data\gps\avgani.paa"];
		_map_loaded = 1;
		_map_size = 5120;
	};
if (_map_name == "beketov" and _map_loaded == 0) then
	{
		_veh setObjectTextureGlobal [7, "qin_titus\data\gps\beketov.paa"];
		_map_loaded = 1;
	};
if (_map_name == "Bootcamp_ACR" and _map_loaded == 0) then
	{
		_veh setObjectTextureGlobal [7, "qin_titus\data\gps\bukovina.paa"];
		_map_loaded = 1;
	};
if (_map_name == "Woodland_ACR" and _map_loaded == 0) then
	{
		_veh setObjectTextureGlobal [7, "qin_titus\data\gps\bystrice.paa"];
		_map_loaded = 1;
	};
if (_map_name == "caribou" and _map_loaded == 0) then
	{
		_veh setObjectTextureGlobal [7, "qin_titus\data\gps\caribou.paa"];
		_map_loaded = 1;
		_map_size = 8175;
	};
if (_map_name == "mbg_celle2" and _map_loaded == 0) then
	{
		_veh setObjectTextureGlobal [7, "qin_titus\data\gps\celle.paa"];
		_map_loaded = 1;
		_map_size = 12287;
	};
if (_map_name == "dingor" and _map_loaded == 0) then
	{
		_veh setObjectTextureGlobal [7, "qin_titus\data\gps\dingor.paa"];
		_map_loaded = 1;
	};
if (_map_name == "cmr_cicada" and _map_loaded == 0) then
	{
		_veh setObjectTextureGlobal [7, "qin_titus\data\gps\cicada.paa"];
		_map_loaded = 1;
	};
if (_map_name == "eden" and _map_loaded == 0) then
	{
		_veh setObjectTextureGlobal [7, "qin_titus\data\gps\everon.paa"];
		_map_loaded = 1;
		_map_size = 12800;
	};
if (_map_name == "fallujah" and _map_loaded == 0) then
	{
		_veh setObjectTextureGlobal [7, "qin_titus\data\gps\fallujah.paa"];
		_map_loaded = 1;
	};
if (_map_name == "IslaDuala3" and _map_loaded == 0) then
	{
		_veh setObjectTextureGlobal [7, "qin_titus\data\gps\isla_duala.paa"];
		_map_loaded = 1;
	};
if (_map_name == "cain" and _map_loaded == 0) then
	{
		_veh setObjectTextureGlobal [7, "qin_titus\data\gps\kolgujev.paa"];
		_map_loaded = 1;
		_map_size = 12800;
	};
if (_map_name == "kulima" and _map_loaded == 0) then
	{
		_veh setObjectTextureGlobal [7, "qin_titus\data\gps\kulima.paa"];
		_map_loaded = 1;
	};
if (_map_name == "pja314" and _map_loaded == 0) then
	{
		_veh setObjectTextureGlobal [7, "qin_titus\data\gps\leskovets.paa"];
		_map_loaded = 1;
	};
if (_map_name == "lingor3" and _map_loaded == 0) then
	{
		_veh setObjectTextureGlobal [7, "qin_titus\data\gps\lingor.paa"];
		_map_loaded = 1;
	};
if (_map_name == "lithium" and _map_loaded == 0) then
	{
		_veh setObjectTextureGlobal [7, "qin_titus\data\gps\lithium.paa"];
		_map_loaded = 1;
	};
if (_map_name == "abel" and _map_loaded == 0) then
	{
		_veh setObjectTextureGlobal [7, "qin_titus\data\gps\malden.paa"];
		_map_loaded = 1;
	};
if (_map_name == "plr_mana" and _map_loaded == 0) then
	{
		_veh setObjectTextureGlobal [7, "qin_titus\data\gps\mana.paa"];
		_map_loaded = 1;
		_map_size = 12800;
	};
if (_map_name == "noe" and _map_loaded == 0) then
	{
		_veh setObjectTextureGlobal [7, "qin_titus\data\gps\nogova.paa"];
		_map_loaded = 1;
		_map_size = 12800;
	};
if (_map_name == "panthera3" and _map_loaded == 0) then
	{
		_veh setObjectTextureGlobal [7, "qin_titus\data\gps\panthera.paa"];
		_map_loaded = 1;
	};
if (_map_name == "Winthera3" and _map_loaded == 0) then
	{
		_veh setObjectTextureGlobal [7, "qin_titus\data\gps\panthera_winter.paa"];
		_map_loaded = 1;
	};
if (_map_name == "cartercity" and _map_loaded == 0) then
	{
		_veh setObjectTextureGlobal [7, "qin_titus\data\gps\pecher.paa"];
		_map_loaded = 1;
		_map_size = 8192;
	};
if (_map_name == "FDF_Isle1_a" and _map_loaded == 0) then
	{
		_veh setObjectTextureGlobal [7, "qin_titus\data\gps\podagorsk.paa"];
		_map_loaded = 1;
	};
if (_map_name == "porto" and _map_loaded == 0) then
	{
		_veh setObjectTextureGlobal [7, "qin_titus\data\gps\porto.paa"];
		_map_loaded = 1;
	};
if (_map_name == "ProvingGrounds_PMC" and _map_loaded == 0) then
	{
		_veh setObjectTextureGlobal [7, "qin_titus\data\gps\proving_grounds.paa"];
		_map_loaded = 1;
	};
if (_map_name == "sara" and _map_loaded == 0) then
	{
		_veh setObjectTextureGlobal [7, "qin_titus\data\gps\sahrani.paa"];
		_map_loaded = 1;
	};
if (_map_name == "Sara_dbe1" and _map_loaded == 0) then
	{
		_veh setObjectTextureGlobal [7, "qin_titus\data\gps\sahrani.paa"];
		_map_loaded = 1;
	};
if (_map_name == "saru" and _map_loaded == 0) then
	{
		_veh setObjectTextureGlobal [7, "qin_titus\data\gps\sarugao.paa"];
		_map_loaded = 1;
	};
if (_map_name == "Shapur_BAF" and _map_loaded == 0) then
	{
		_veh setObjectTextureGlobal [7, "qin_titus\data\gps\shapur.paa"];
		_map_loaded = 1;
	};
if (_map_name == "SchmalfeldenXHV" and _map_loaded == 0) then
	{
		_veh setObjectTextureGlobal [7, "qin_titus\data\gps\schmalfelden.paa"];
		_map_loaded = 1;
		_map_size = 5120;
	};
if (_map_name == "stratis" and _map_loaded == 0) then
	{
		_veh setObjectTextureGlobal [7, "qin_titus\data\gps\stratis.paa"];
		_map_loaded = 1;
	};
if (_map_name == "thirsk" and _map_loaded == 0) then
	{
		_veh setObjectTextureGlobal [7, "qin_titus\data\gps\thirsk.paa"];
		_map_loaded = 1;
	};
if (_map_name == "thirskW" and _map_loaded == 0) then
	{
		_veh setObjectTextureGlobal [7, "qin_titus\data\gps\thirsk_winter.paa"];
		_map_loaded = 1;
	};
if (_map_name == "utes" and _map_loaded == 0) then
	{
		_veh setObjectTextureGlobal [7, "qin_titus\data\gps\utes.paa"];
		_map_loaded = 1;
	};
if (_map_name == "vt5" and _map_loaded == 0) then
	{
		_veh setObjectTextureGlobal [7, "qin_titus\data\gps\vt5.paa"];
		_map_loaded = 1;
	};
if (_map_name == "zargabad" and _map_loaded == 0) then
	{
		_veh setObjectTextureGlobal [7, "qin_titus\data\gps\zargabad.paa"];
		_map_loaded = 1;
	};
if (_map_name == "carter_wrp" and _map_loaded == 0) then
	{
		_veh setObjectTextureGlobal [7, "qin_titus\data\gps\zarzibash.paa"];
		_map_loaded = 1;
		_map_size = 8192;
	};
if (_map_loaded == 0) then
	{
		_veh setObjectTextureGlobal [7, "qin_titus\data\gps\no_data.paa"];
		_veh setObjectTextureGlobal [8, "#(argb,8,8,3)color(0.86,0.14,0.02,0,ca)"];

	};

[_veh, _map_size, _map_loaded] spawn _showPos;