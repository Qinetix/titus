_veh = _this select 0;
_map_size = _this select 1;
_map_loaded = _this select 2;

while {alive _veh and _map_loaded == 1} do {
		_posX = [(getpos _veh) select 0,0] call BIS_fnc_cutDecimals;
		_posY = [(getpos _veh) select 1,0] call BIS_fnc_cutDecimals;

		_showX = _posX/_map_size;
		_showY = _posY/_map_size;

		//hint parseText format ["mapsize: %1 <br/> pos_x: %2 <br/> pos_y: %3 <br/>show_X: %4 <br/>show_Y: %5 <br/>Map: %6", _map_size, _posX, _posY, _showX, _showY, _map_name];

		_veh animate ["map_pos_x",_showX];
		_veh animate ["map_pos_y",_showY];

		sleep 1;
	};