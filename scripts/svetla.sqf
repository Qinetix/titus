private ["_vehicle","_lightleft","_lightright"];

_vehicle = _this select 0;

waitUntil {(_vehicle getVariable ["lights_on", false])};

while {(_vehicle getVariable ["lights_on", false]) AND canMove _vehicle} do
	{

		_lightleft = "#lightpoint" createVehicle getpos _vehicle;
		_lightleft setLightColor [0, 0, 255];
		_lightleft setLightBrightness 1;
		_lightleft setLightAmbient [0,0,0.5];
		_lightleft lightAttachObject [_vehicle, [0, 1, 1]];

		_lightleft setLightBrightness 0.25;
		_lightleft setLightColor [0, 0, 255];
		_vehicle animate ["police_lights_l", 1];

		sleep 0.1;
		_lightleft setLightBrightness 0;
		_vehicle animate ["police_lights_l", 0];

		sleep 0.1;
		_lightleft setLightBrightness 0.25;
		_lightleft setLightColor [0, 0, 255];
		_vehicle animate ["police_lights_l", 1];

		sleep 0.1;
		_lightleft setLightBrightness 0;
		_lightleft setLightBrightness 0;
		_vehicle animate ["police_lights_l", 0];
		deleteVehicle _lightleft;

		sleep 0.3;

		_lightright = "#lightpoint" createVehicle getpos _vehicle;
		_lightright setLightColor [0, 0, 255];
		_lightright setLightAmbient [0,0,0.5];
		_lightright lightAttachObject [_vehicle, [0, 1, 1]];

		_lightright setLightBrightness 0.25;
		_lightright setLightColor [0, 0, 255];
		_vehicle animate ["police_lights_r", 1];

		sleep 0.1;
		_lightright setLightBrightness 0;
		_vehicle animate ["police_lights_r", 0];

		sleep 0.1;
		_lightright setLightBrightness 0.25;
		_lightright setLightColor [0, 0, 255];
		_vehicle animate ["police_lights_r", 1];

		sleep 0.1;
		_lightright setLightBrightness 0;
		_lightright setLightBrightness 0;
		_vehicle animate ["police_lights_r", 0];
		deleteVehicle _lightright;

		sleep 0.3;
};

if (!canMove _vehicle) exitWith{};

[_vehicle] spawn svetla;
