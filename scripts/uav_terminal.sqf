private ["_has_GPS", "_termianlclass", "_unit", "_veh", "_has_terminal", "_position", "_in_position", "_terminal_done", "_inVehicle","_gotGPSonce"];

_veh = _this select 0;
_position = _this select 1;
_unit = _this select 2;

_has_GPS = false;
_termianlclass = "";
_has_terminal = false;
_in_position = false;
_terminal_done = false;
_inVehicle = true;
_gotGPSonce = false;


//hint parseText format ["position: %1", _position];

while {_inVehicle} do {
		if (_unit == commander _veh) then
			{
				_gotGPSonce = false;
				_in_position = true;
			};
		if (_in_position AND !(_terminal_done)) then
			{
				if ("ItemGPS" in assignedItems _unit OR "ItemGPS" in items _unit) then
					{
						_has_GPS = true;
					};
				switch (side _unit) do {
						case WEST: {_termianlclass = "B_UavTerminal";};
						case EAST: {_termianlclass = "O_UavTerminal";};
						case INDEPENDENT: {_termianlclass = "I_UavTerminal";};
						case CIVILIAN: {_termianlclass = "C_UavTerminal";};
					};

				if ((_termianlclass in assignedItems _unit)) then
					{
						_has_terminal = true;
					};

				_unit addItem _termianlclass;
				_unit assignItem _termianlclass;

				_terminal_done = true;
			};
		if (!(_unit in _veh) OR !(_unit == commander _veh)) then
					{
						_in_position = false;
						if !(_has_terminal) then
							{
								_unit unassignItem _termianlclass;
								_unit removeItem _termianlclass;
							};
						if (_has_GPS AND !("ItemGPS" in items _unit) AND !(_gotGPSonce)) then
							{
								_unit addItem "ItemGPS";
								_unit assignItem "ItemGPS";
								_gotGPSonce = true;
							};
						_terminal_done = false;
						if (!(_unit in _veh)) then
							{
								_inVehicle = false;
							};
					};
		sleep 1;
	};