TITUS for Arma 3

TITUS (Tactical Infantry Transport & Utility System) is 6x6 MRAP vehicle developed by Nexter with cooperation of Tatra.
Read more information about this vehicle <a href="http://www.armyrecognition.com/french_army_france_wheeled_armoured_vehicle_uk/titus_nexter_tactical_infantry_transport_6x6_armoured_vehicle_utility_system_technical_data_sheet.html" target="_blank">here</a>.

Features: Tatra suspension, animated doors, hatches, GPS, UAV terminal for commander, slat cage, camo net, windows grilles, police version with working lights and sirens
<br><br>
<a href="http://ofp-csec.info/images/gallery/titus/" target="_blank"><b>Work in progress gallery</b>
<br><br>
<img src="http://ofp-csec.info/images/gallery/titus/images/titus_wip_62.jpg" width="400"> <img src="http://ofp-csec.info/images/gallery/titus/images/titus_wip_54.jpg" width="400"> 
<br><br>
<img src="http://ofp-csec.info/images/gallery/titus/images/titus_wip_33.jpg" width="400"> <img src="http://ofp-csec.info/images/gallery/titus/images/titus_wip_34.jpg" width="400"></a>